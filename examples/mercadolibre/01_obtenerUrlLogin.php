#!/usr/bin/php
<?php
include('../autoload.php');

$keyApp= '9999999999999999';
$secretApp= 'Abc..................AAAA';
$testMode= false;

$miapp= new Mercadolibre( 
	$keyApp,
	$secretApp, 
	'mex', 
	$testMode
);

echo "\nLenguajes Soportados:\n";
foreach( $miapp->getLangDb() as $key=>$val )
	echo "\n[". $key. "]\t". ucfirst($val["lang"]). " => ". $val["name"];
echo "\n";

$link= $miapp->getLoginUrl();

if( $miapp->getError() )
	echo "\nError: ". $miapp->getError();
else {
	/*
	Te recomendamos seguir y comprender estos pasos:

	1) Obtenemos y mostramos el Link para asociar a nuestro usuario con nuestra APP de mercadolibre.
	2) Cuando el usuario acceda al link y pulse "PERMITIR" nos retornara la variable "code", que es el *Codigo de Autorizacion*
	3) Al detectarse la variable $_GET["code"], se llamara a una funcion especial que nos permitira finalizar la sincronizacion.
	4) Guardamos el code y la informacion obtenida de esta sincronizacion.
	*/
	if( isset($_GET["code"]) ) { # capturamos el codigo de autorizacion
		$urlRedirect= 'https://mipagina.com';
		$miapp->getAutorization(
			$_GET["code"], 
			$urlRedirect
		);

		if( $this->getError() )
			echo "\nError: ". $this->getError();
		else {
			$r= $miapp->getRespuesta();
			echo "\n\nToken: ". $r["token"];
			echo "\nExpira: ". $r["expira"]. ' -- '. date("d/m/Y, H:i:a s", $r["expira"]);
			echo "\nToken for Refresh: ". $r["token_refresh"];

			/*
			guardaremos el access token para obtener el ID del usuario
			*/
			$miapp->setToken($r["token"], $r["token_refresh"]);
			$miapp->getMyInfo();

			if( $miapp->getError() )
				echo $miapp->getError();
			else {
				$user= $miapp->getRespuesta();

				echo "ID User: ". $user["id"];
				print_r($user); # id, nickname, email, permalink, etc..

				/*
				RECOMENDACION: guarda el "token", "token_refresh", "expira" y "id del usuario", en tu Base de Datos preferida
				ya que las token solo duran 6 HORAS y tendras que pedir una nueva token a NIVEL DE CONSOLA, vaya, el usuario
				no tiene que intervenir ni aceptan ninguna ventana, solo debes pedir una nueva token a mercado antes de que
				la actual caduque, para esto deberas enviar la VIEJA TOKEN.

				Te recomiendo seguir el ejemplo: 02_obtenerToken.php
				*/
			}
		}
	}
	else { # mostramos el boton para ir a la APP
		echo "\nClic para ir a mi App.";
		echo "\n\n***********************************************\n*\t";
		echo $link;
		echo "\n***********************************************";
		echo "\n\n**************** IMPORTANTE:\n";
		echo "Recuerda que esta URL te llevara al requerimiento de Permisos para el Usuario que utilizara tu App, pero deberas integrar en tu Pagina Web \n";
		echo "(callBack) la capacidad te capturar el *Codigo de Autorizacion* que obtendras tras llamar al enlace ya mencionado, este *codigo de \n";
		echo "autorizacion* te servira para despues llamar la clase *getToken()* y ahora si obtener un Token Real.\n\n";
		echo "Cuando el usuario le de en \"PERMITIR\" a tu App, Mercadolibre te retornada el *Codigo de Autorizacion* a una ruta mas o menos asi: \n";
		echo "http://DireccionWeb_Configurada_Callback_com?code=CODIGO_MAGICO\n\nRecomendacion?: Guarda el *Codigo de Autorizacion* en tu Base de Datos.";
	}
}
unset($link);

echo "\n\n";
echo "Fin del programa...\n\n";
?>