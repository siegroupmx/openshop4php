#!/usr/bin/php
<?php
include('../autoload.php');

$keyApp= '9999999999999999';
$secretApp= 'Abc..................AAAA';
$testMode= false;

$miapp= new Mercadolibre( 
	$keyApp, 
	$secretApp, 
	'mex', 
	$testMode
);

if( $miapp->getError() )
	echo "\nError: ". $miapp->getError();
else {
	/*
	En este punto debiste obtener un access token al momento que el usuario permitio sincronizar la APP (ejemplo: 01_obtenerUrlLogin.php).
	Dicho Token debiste guardarlo en tu BDD y podras usarlo por un lapso de 6 HORAS, en este punto es donde deberas volver a pedir un nuevo token
	a nivel de CONSOLA (sin que el usuairo intervenga).

	Te recomiendo generar un proceso CRON para que cada 5 horas pida un refresqueo de la TOKEN y se extienda por 6 horas mas.
	El siguiente codigo te servira para crear tu Script para refrescar una token que esta por vencer.
	*/

	$token= 'La_Token';
	$refresh= 'El_Refresh_Token';

	$miapp->setToken($token, $refresh); # seteamos Tokens antiguas
	$miapp->createToken(); # creamos nueva apartir de las existentes

	if( $miapp->getError() )
		echo "\n[ERROR] - ". $miapp->getError();
	else {
		$r= $miapp->getRespuesta();

		echo 'Nuevas tokens<br>';
		print_r($r);
		/**
		* Array(
		*	"token"=>"_nueva_token_", 
		*	"token_refresh"=>"_nueva_refresh_token_", 
		*	"expira"=>"_linuxtime_fecha_expiradion_", 
		* )
		*/
	}
}

echo "\n\n";
echo "Fin del programa...\n\n";
?>