#!/usr/bin/php
<?php
include('../autoload.php');

$keyApp= '9999999999999999';
$secretApp= 'Abc..................AAAA';
$testMode= false;

$miapp= new Mercadolibre( 
	$keyApp, 
	$secretApp, 
	'mex', 
	$testMode
);

if( $miapp->getError() )
	echo "\nError: ". $miapp->getError();
else {
	$token= '_token_';
	$refresh= '_refresh_token_';
	
	$miapp->setToken($token, $refresh); # seteamos Tokens antiguas
	$miapp->getCatalogos();

	if( $miapp->getError() )
		echo "\n[ERROR] - ". $miapp->getError();
	else {
		$f= $miapp->getRespuesta();

		/**
		* Es importante entender que Mercadolibre requiere que publiques articulos en las Categorias que tienen derecho a almacenar publicaciones, 
		* vaya, las categorias principales No pueden almacenar publicaciones, sino aquellas que esten AL FINAL del arbol.
		* Por lo cual deberas navegar de categoria en categoria hasta el ultimo punto para obtener "ese ID" y ahora si Publicar tu producto en
		* dicha Categoria.
		*
		* Lo que guardaras en tu base de datos es el ID de la categoria.
		*/
		
		echo '- Nivel 1<br>';
		foreach( $r as $key=>$val ) {
			echo '<br>ID: '. $val->id. ', Nombre: '. $val->name;

			if( $val->children_categories ) { # si tiene nodos internos esta categorias
				$miapp->getCatalogos($val->id); # la consultamos
				$a= $miapp->getRespuesta();

				echo '<br>- Nivel 2 de: ['. $val->name. ']';
				foreach( $a as $key2=>$val2 ) {
					if( $val2->children_categories ) { # si tiene nodos internos esta categorias
						$miapp->getCatalogos($val2->id); # la consultamos
						$b= $miapp->getRespuesta();

						echo '<br>- Nivel 3 de: ['. $val2->name. ']';
						foreach( $b as $key3=>$val3 ) {
							/**
							* ciclo maximo te recomiendo de 6 niveles de busqueda...
							*/
						}
					}
					else
						echo '<br>--> ['. $val2->id. '] - '. $val2->name;
				}
			}
			else
				echo '<br>--> ['. $val->id. '] - '. $val->name;
		}
	}
}

echo "\n\n";
echo "Fin del programa...\n\n";
?>