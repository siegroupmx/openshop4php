#!/usr/bin/php
<?php
include('../autoload.php');

$keyApp= '9999999999999999';
$secretApp= 'Abc..................AAAA';
$testMode= false;

$miapp= new Mercadolibre( 
	$keyApp, 
	$secretApp, 
	'mex', 
	$testMode
);

if( $miapp->getError() )
	echo "\nError: ". $miapp->getError();
else {
	$token= '_token_';
	$refresh= '_refresh_token_';
	$idItem= '_nuevo_articulo_guardado_anteriormente_';

	/**
	* Este array es la configuracion minima de lo que debe contener tu Item, puedes obtener mas informacion en: 
	* https://developers.mercadolibre.com.mx/es_ar/producto-autenticacion-autorizacion/publica-productos
	*/
	$item=array(
		"title"=>"_titulo_del_producto_", 
		"category_id"=>"_ID_Categoria_", 
		"price"=>"_precio_", 
		"currency_id"=>"_tipo_moneda_", 
		"available_quantity"=>"_unidades_existencia_", 
		"buying_mode"=>"buy_it_now", 
		"listing_type_id"=>"gold_special", 
		"condition"=>"_condicion_del_producto: new o used", 
		"description"=>"_Descripcion_del_producto_en_texto_plano", 
		"warranty"=>"_tiempo_de_garantia_", 
		"tags"=>array("0"=>"immediate_payment"), # pago con MercadoPagos
		"shipping"=>array( 
			"mode"=>"me2", 
			"local_pick_up"=>true, 
			"free_shipping"=>false, 
			"free_method"=>array(
				"id"=>73328, 
				"rule"=>array(
					"free_mode"=>"country", 
					"value"=>NULL
				)
			) 
		),  # envio por Mercado Envios
		"pictures"=>array( 
			0=>array(
				"source"=>"https://img.moneybox.com.mx/almacen/". $buf["ID"]. "/". ($i+1). "/r5"
			)
		)
	);
	
	$miapp->setToken($token, $refresh); # seteamos Tokens antiguas
	$miapp->setItemId($idItem);
	$miapp->modItem($item);

	if( $miapp->getError() )
		echo "\n[ERROR] - ". $miapp->getError();
	else {
		$f= $miapp->getRespuesta();

		echo 'Item actualizado, datos de respuesta:<br>';
		print_r($r);
	}
}

echo "\n\n";
echo "Fin del programa...\n\n";
?>