#!/usr/bin/php
<?php
include('../autoload.php');

$keyApp= '9999999999999999';
$secretApp= 'Abc..................AAAA';
$testMode= false;

$miapp= new Mercadolibre( 
	$keyApp, 
	$secretApp, 
	'mex', 
	$testMode
);

if( $miapp->getError() )
	echo "\nError: ". $miapp->getError();
else {
	$token= '_token_';
	$refresh= '_refresh_token_';
	$idUser='999999';
	
	$miapp->setToken($toke, $refresh); # seteamos Tokens antiguas
	$miapp->setUser($idUser);
	$miapp->setItemId(NULL); # seteamos a nulo para prevenir
	$miapp->setPagina(0); # pagina 0 (cero)
	$miapp->getItem();

	if( $miapp->getError() )
		echo "\n[ERROR] - ". $miapp->getError();
	else {
		echo 'Listamos todos los Items:<br>';

		$r= $miapp->getRespuesta();
		$pagina_inicio= 0;
		$pagina_fin= 0;

		do {
			if( !$pagina_inicio ) # iniciando
				$pagina_fin= ceil($r->paging->total/$miapp->getLimiter()); # seteamos total pagina
			else {
				$miapp->setPagina( ($pagina_inicio+1) );
				$miapp->getItem();
				$r= $miapp->getRespuesta();
			}

			echo "\n\n--- Pagina ". ($pagina_inicio+1). " / ". $pagina_fin;

			foreach( $r->results as $key=>$val ) {
				echo "\n[". $key. "] --> ". $val;
			}
			$pagina_inicio++;
		}while(($pagina_inicio+1)<=$pagina_fin);
	}
}

echo "\n\n";
echo "Fin del programa...\n\n";
?>