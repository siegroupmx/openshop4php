#!/usr/bin/php
<?php
include('../autoload.php');

$keyApp= '9999999999999999';
$secretApp= 'Abc..................AAAA';
$testMode= false;

$miapp= new Mercadolibre( 
	$keyApp, 
	$secretApp, 
	'mex', 
	$testMode
);

if( $miapp->getError() )
	echo "\nError: ". $miapp->getError();
else {
	$token= '_token_';
	$refresh= '_refresh_token_';
	$idUser='999999';
	
	$miapp->setToken($toke, $refresh); # seteamos Tokens antiguas
	$miapp->setUser($idUser);
	$miapp->setItemId(NULL); # seteamos a nulo para prevenir
	$miapp->getItem();
	$articulos=array();

	if( $miapp->getError() )
		echo "\n[ERROR] - ". $miapp->getError();
	else {
		echo "Listamos todos los Items:\n";

		$r= $miapp->getRespuesta();
		$miapp->setPagina(0); # pagina 0 (cero)
		$pagina_inicio= 0;
		$pagina_fin= 0;

		echo "\n\n.... Recopilando datos de tus articulos...\n";

		do {
			if( !$pagina_inicio ) # iniciando
				$pagina_fin= ceil($r->paging->total/$miapp->getLimiter()); # seteamos total pagina
			else {
				$miapp->setPagina( ($pagina_inicio+1) );
				$miapp->getItem();
				$r= $miapp->getRespuesta();
			}

			foreach( $r->results as $key=>$val ) {
				$articulos[]= $val; # guardamos
			}
			$pagina_inicio++;
		}while(($pagina_inicio+1)<=$pagina_fin);
	}

	# eliminando todo
	if( !count($articulos) )
		echo "\n\n[ERROR] No tienes articulos en tu tienda..";
	else {
		echo "\n\n---- Eliminando ". count($articulos). " articulos\n";
		foreach( $articulos as $key=>$val ) {
			echo "\nArticulo ". ($key+1). " - ". $val. " ===> ";
			$miapp->setItemId($val); # establecemos objetivo item
			$miapp->closeItem(); # detenemos publicacion
			$miapp->delItem(); # eliminamos
			$r= $miapp->getRespuesta();

			if( $r->id ) 	echo "DONE";
			else 	echo "ERROR";
		}
	}
}

echo "\n\n";
echo "Fin del programa...\n\n";
?>