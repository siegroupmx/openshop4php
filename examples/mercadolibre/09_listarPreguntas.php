#!/usr/bin/php
<?php
include('../autoload.php');

$keyApp= '9999999999999999';
$secretApp= 'Abc..................AAAA';
$testMode= false;

$miapp= new Mercadolibre( 
	$keyApp, 
	$secretApp, 
	'mex', 
	$testMode
);

if( $miapp->getError() )
	echo "\nError: ". $miapp->getError();
else {
	$token= '_token_';
	$refresh= '_refresh_token_';
	$idUser='999999';
	
	$miapp->setToken($token, $refresh); # seteamos Tokens antiguas
	$miapp->setItemId($idItem);
	$miapp->getCuestions();

	if( $miapp->getError() )
		echo "\n[ERROR] - ". $miapp->getError();
	else {
		$r= $miapp->getRespuesta();

		echo "\nPreguntas del articulo ". $idItem;

		foreach( $r->questions as $key=>$val ) {
			echo "\n\nUsuario ". $val->from->id. " dice:";
			echo "\n-- [". $val->date_created. "] ". $val->text;
			echo "\n-- Estado: ". $val->status;
			echo "\n-- ID Pregunta: ". $val->id;
		}
	}
}

echo "\n\n";
echo "Fin del programa...\n\n";
?>