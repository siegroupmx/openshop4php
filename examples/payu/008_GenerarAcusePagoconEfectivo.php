#!/usr/bin/php
<?php
include("../../autoload.php");

session_start();

$payuUser='pRRXKOl8ikMmt9u';
$payuKey='4Vj8eK4rloUd272L48hsrarnUA';
$payuId='512324';
$payuMerchId='508029';
$pais= 'mx';
$sandbox= true; # pruebas activo

$miapp= new PayU(
	$payuUser, 
	$payuKey, 
	$payuId, 
	$payuMerchId, 
	$pais, 
	$sandbox
);

if( $miapp->getError() )	echo "\nError: ". $miapp->getError();
else {
	# 1- establecendo el metodo de pago del cliente - VISA/MASTERCAD/AMEX
	$tipo= 'OXXO';
	$miapp->setPaymentMethod($tipo);

	if( $miapp->getError() )	echo "\nError: ". $miapp->getError();
	else {
		# si no conocer el template array descomenta lo siguiente
		# $data= $miapp->getTemplatePayment("tarjeta"); # obtenemos template antes de llenar

		$fecha= time();
		$miapp->setMoneda("MXN"); # estableciendo moneda de cobro

		$idnum= '5434455455'; # identificador visual en mi sistema
		$id_track= 'a8ajaaj8aj'; # identificador unico irrepetible de mi BDD
		$total= 1160;
		$calle= 'Rosalinda';
		$numext= '110';
		$colonia= 'La Franja';
		$ciudad= 'Reynosa';
		$estado= 'Tamaulipas';
		$pais= 'MX';
		$cp= '88700';
		$telefono= '8990000000';
		$email= 'siegroupmx@gmail.com';
		$rfc= 'XAXX010101000';
		$data=array(
			"type"=>$tipo,
			"orden_id"=>$idnum, # numero de orden del pedido del sistema
			"orden_expiracion"=>date( "Y-m-d\TH:i:s", ($fecha+86400)), # expiracion de la orden, para oxxo y seven eleven (24 horas)
			"nombre"=>"Compra ". $idnum. " el ". date("d/m/Y, g:i a", $fecha), # nombre de la orden
			"notify"=>"https://www.miempresa.com/aquiguardare_laorden/". $id_track. "/", # url donde el cliente puede ver la orden generada en el sistema
			"total"=>$total, # es el total a pagar de la orden
			"cliente"=>array(
				"id"=>"9999", # id del cliente en mi sistema
				"nombre"=>"Cliente de Prueba", 
				"email"=>$email, # un correo real, para recibir la notificacion
				"telefono"=>$telefono,
				"nit"=>$rfc,  # numero de identificacion tributario de gobierno o dejar en 0 (cero)
				"calle"=>$calle. ( ($numext && strcmp($numext, "-")) ? " ".$numext:""), # direccion personal
				"colonia"=>$colonia, # direccion personal
				"ciudad"=>$ciudad, # direccion personal
				"estado"=>$estado, # direccion personal
				"pais"=>$pais, # direccion personal
				"cp"=>$cp # direccion personal
				),
			"embarque"=>array(
				"calle"=>$calle, # direccion del envio paquete
				"colonia"=>$colonia, # direccion del envio paquete
				"ciudad"=>$ciudad, # direccion del envio paquete
				"estado"=>$estado, # direccion del envio paquete
				"pais"=>$pais, # direccion del envio paquete
				"cp"=>$cp, # direccion del envio paquete
				"telefono"=>$telefono
				), 
			"tarjeta"=>array(
				"id"=>"232333", # id de la tarjeta en el sistema
				"nombre"=>"Cliente de Prueba", 
				"email"=>$email, 
				"fechanacimiento"=>"1984-12-14", # yyyy-mm-dd
				"telefono"=>$telefono, 
				"nit"=>$rfc, 
				"calle"=>$calle, # direccion del envio paquete
				"colonia"=>$colonia, # direccion del envio paquete
				"ciudad"=>$ciudad, # direccion del envio paquete
				"estado"=>$estado, # direccion del envio paquete
				"pais"=>$pais, # direccion del envio paquete
				"cp"=>$cp, # direccion del envio paquete
				"card_numero"=>"4772910000000008", # numero tarjeta a 16 digitos
				"card_codigo"=>"321", # 3 digitos verificadores
				"card_expiracion"=>"2020/12" # expiracion: YYYY/mm
				), 
			"pais"=>$pais, # 2 caracteres para indicar pais de origen del pago
			"ip"=>"192.168.1.99", # ip del que genera el pago
			"navegador"=>"Firefox a.B"
		);

		$miapp->sendPayment($data);

		if( $miapp->getError() )	echo "\nError: ". $miapp->getError();
		else {
			echo "\n\nExito..\n";
			$r= $miapp->getRespuesta();
			
			echo "\n\nEstado Payu: ". $r->code;
			echo "\nEstado: ". $r->transactionResponse->state;
			if( !strcmp($r->transactionResponse->state, "PENDING") ) # pendiente - siempre para pagos en efectivo
				echo "\nRazon del Pendiente: ". $r->transactionResponse->pendingReason;  # AWAITING_NOTIFICATION -> esperando el pago del usuario
			echo "\nOrden Id: ". $r->transactionResponse->orderId;
			echo "\nTransaccion Hash: ". $r->transactionResponse->transactionId;
			# echo "\nCodigo Autorizacion: ". $r->transactionResponse->authorizationCode; # pagos en efectivo, no obtendra autorizacion
			echo "\nCodigo Respuesta: ". $r->transactionResponse->responseCode;
			# echo "\nFecha Operacion: ". $r->transactionResponse->operationDate; # pagos en efectivo, no obtendra fecha de autorizacion del pago
			echo "\nExpiracion del Pago: ". $r->transactionResponse->extraParameters->entry[0]->date;
			echo "\nPDF PayU: ". $r->transactionResponse->extraParameters->entry[1]->string[1];
			echo "\nHTML PayU: ". $r->transactionResponse->extraParameters->entry[4]->string[1];
			echo "\nCodigo de Barras: ". $r->transactionResponse->extraParameters->entry[2]->string[1];
			echo "\n\n";
			print_r($r);

			$miapp->setOrderId($r->transactionResponse->orderId); # guardamos la Orden generada por PayU
			$miapp->setTransactionId($r->transactionResponse->transactionId); # guardamos el Hash de la Transaccion generada por PayU
			$miapp->setTransactionStatus($r->transactionResponse->state); # establece el estado de la Transaccion (REJECTED, APPROVED, DECLINED, PENDING)
			$miapp->setPathReceipt($path);
			$miapp->setReceiptName("prueba");
			$miapp->setEmisorData($empresa);
			$miapp->setReceptorData($cliente);
			$miapp->setCobroData($cobro);

			/**
			* Success Example
			*/
			/*
			SimpleXMLElement Object
			(
			    [code] => Success													<----- IMPORTANTE estado general: SUCCESS o ERROR
			    [transactionResponse] => SimpleXMLElement Object
			        (
			            [orderId] => 845857653										<----- IMPORTANTE la Orden dentro de PayU
			            [transactionId] => cd082a30-b977-4db8-bf30-1ad8dd283763		<----- IMPORTANTE la Transaccion dentro de PayU
			            [state] => PENDING 											<----- IMPORTANTE para efectivo siempre sera PENDING
			            [pendingReason] => AWAITING_NOTIFICATION					<----- Razon del PENDING
			            [responseCode] => PENDING_TRANSACTION_CONFIRMATION			<----- Codigo de respuesta del PENDING
			            [extraParameters] => SimpleXMLElement Object
			                (
			                    [entry] => Array
			                        (
			                            [0] => SimpleXMLElement Object
			                                (
			                                    [string] => EXPIRATION_DATE
			                                    [date] => 2019-07-08T12:56:26		<---- expiracion del Pago
			                                )

			                            [1] => SimpleXMLElement Object
			                                (
			                                    [string] => Array
			                                        (
			                                            [0] => URL_PAYMENT_RECEIPT_PDF		<---- PDF de la linea de captura
			                                            [1] => https://sandbox.checkout.payulatam.com/ppp-web-gateway-payu/receipt?vid=845857653Ycd082a30b9774dbY52de53e71f4a8a3
			                                        )

			                                )

			                            [2] => SimpleXMLElement Object
			                                (
			                                    [string] => Array
			                                        (
			                                            [0] => BAR_CODE 					<---- Codigo de la linea
			                                            [1] => 27000845857653201907080001160002
			                                        )

			                                )

			                            [3] => SimpleXMLElement Object
			                                (
			                                    [string] => REFERENCE 						<---- la Orden de Compra interna de PayU
			                                    [int] => 845857653
			                                )

			                            [4] => SimpleXMLElement Object
			                                (
			                                    [string] => Array
			                                        (
			                                            [0] => URL_PAYMENT_RECEIPT_HTML		<---- HTML de la linea de captura
			                                            [1] => https://sandbox.checkout.payulatam.com/ppp-web-gateway-payu/app?vid=845857653Ycd082a30b9774dbY52de53e71f4a8a3
			                                        )

			                                )

			                            [5] => SimpleXMLElement Object
			                                (
			                                    [string] => Array
			                                        (
			                                            [0] => PAYMENT_WAY_ID
			                                            [1] => 1
			                                        )

			                                )

			                            [6] => SimpleXMLElement Object
			                                (
			                                    [string] => Array
			                                        (
			                                            [0] => BANK_REFERENCED_CODE
			                                            [1] => CASH
			                                        )

			                                )

			                        )

			                )

			        )

			)


			*/
		}
	}
}

echo "\n\nFin de programa....\n";
exit(0);
?>