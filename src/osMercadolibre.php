<?php
/**
* OpenShop4PHP - edicion Mercadolibre
*
* libreria para conectar al ecommerce de Mercadolibre, autentificar, gestionar productos, preguntas, ordenes y paqueteria.
*
* @version 1.3-stable
* @author Angel Haniel Cantu Jauregui
* @author angel.cantu@sie-group.net
* @author https://www.moneybox.business
* 
*/
class Mercadolibre {
	/**
	* @var string URL del API Mercadolibre
	*/
	const API='https://api.mercadolibre.com';

	/**
	* @var string URL del API Sanbox de Mercadolibre
	*/
	const API_SANDBOX='https://api.mercadolibre.com';

	/**
	* @var string APP Id que puede asignarse por funcion o manualmente
	*/
	const APP_ID='';

	/**
	* @var string APP Secret que puede asignarse por funcion o manualmente
	*/
	const APP_SECRET='';

	/**
	* @var string URL para Redirecciones debe coincidir con la configuracion de Mercado
	*/
	const URL_REDIRECT= '';

	/**
	* @var array lista de URLS del API para Autentificaciones segun el Lenguaje de la APP
	*/
	private $sitesMercado= array(
		"arg"=>array("name"=>"MLA", "url"=>"https://auth.mercadolibre.com.ar", "lang"=>"argentina"), 
		"bol"=>array("name"=>"MBO", "url"=>"https://auth.mercadolivre.com.bo", "lang"=>"bolivia"), 
        "br"=>array("name"=>"MLB", "url"=>"https://auth.mercadolivre.com.br", "lang"=>"brasil"), 
        "chl"=>array("name"=>"MLC", "url"=>"https://auth.mercadolibre.com.cl", "lang"=>"chile"), 
        "col"=>array("name"=>"MCO", "url"=>"https://auth.mercadolibre.com.co", "lang"=>"colombia"), 
        "cr"=>array("name"=>"MCR", "url"=>"https://auth.mercadolibre.com.cr", "lang"=>"costa rica"), 
        "dom"=>array("name"=>"MRD", "url"=>"https://auth.mercadolibre.com.do", "lang"=>"dominicana"), 
        "ec"=>array("name"=>"MEC", "url"=>"https://auth.mercadolibre.com.ec", "lang"=>"ecuador"), 
        "gta"=>array("name"=>"MGT", "url"=>"https://auth.mercadolibre.com.gt", "lang"=>"guatemala"), 
        "hon"=>array("name"=>"MHN", "url"=>"https://auth.mercadolibre.com.hn", "lang"=>"honduras"), 
        "mex"=>array("name"=>"MLM", "url"=>"https://auth.mercadolibre.com.mx", "lang"=>"mexico"), 
        "nic"=>array("name"=>"MNI", "url"=>"https://auth.mercadolibre.com.ni", "lang"=>"nicaragua"), 
        "pan"=>array("name"=>"MPA", "url"=>"https://auth.mercadolibre.com.pa", "lang"=>"panama"), 
        "par"=>array("name"=>"MPY", "url"=>"https://auth.mercadolibre.com.py", "lang"=>"paraguay"), 
        "pru"=>array("name"=>"MPE", "url"=>"https://auth.mercadolibre.com.pe", "lang"=>"peru"), 
        "por"=>array("name"=>"MPT", "url"=>"https://auth.mercadolibre.com.pt", "lang"=>"portugal"), 
        "sal"=>array("name"=>"MSV", "url"=>"https://auth.mercadolibre.com.sv", "lang"=>"alvador"), 
        "uru"=>array("name"=>"MLU", "url"=>"https://auth.mercadolibre.com.uy", "lang"=>"uruguay"), 
        "ven"=>array( "name"=>"MLV", "url"=>"https://auth.mercadolibre.com.ve", "lang"=>"venezuela")
	);

	/**
	* @var string guarda el username o ID usuario
	*/
	private $user=NULL;

	/**
	* @var string codigo del APP ID
	*/
	private $key=NULL;

	/**
	* @var string codigo APP Secret
	*/
	private $secret=NULL;

	/**
	* @var string URL del API a consultar
	*/
	private $apiUrl=NULL;

	/**
	* @var string URL del API de Autentificaciones
	*/
	private $apiAuth=NULL;

	/**
	* @var string nombre del API de Autentificaciones
	*/
	private $apiAuthName=NULL;

	/**
	* @var string nombre asignado para el API de autentificaciones
	*/
	private $wsLang=NULL;

	/**
	* @var boolean valor para indicar si la Sandbox esta habilitada o deshabilitada
	*/
	private $sandbox=false;

	/**
	* @var string mensaje de exito de la ultima transaccion
	*/
	private $sucess=NULL;

	/**
	* @var string codigo del mensaje de exito de la ultima transaccion
	*/
	private $sucess_code=NULL;

	/**
	* @var string mensaje de error de la ultima transaccion
	*/
	private $error=NULL;

	/**
	* @var string codigo del mensaje de error de la ultima transaccion
	*/
	private $error_code=NULL;

	/**
	* @var string mensaje completo del Request enviado
	*/
	private $headerRequest= NULL;

	/**
	* @var string mensaje completo del Response recibido
	*/
	private $headerResponse=NULL;

	/**
	* @var string datos especificamente formados como respuesta final a una consulta exitosa, tambien puede ser un array
	*/
	private $response=NULL;

	/**
	* @var string hash del token
	*/
	private $token=NULL;

	/**
	* @var string hash del token de refresqueo
	*/
	private $token_refresh=NULL;

	/**
	* @var string numero del item en mercado
	*/
	private $item='';

	/**
	* @var intiger paginacion
	*/
	private $pagina=0;

	/**
	* @var intiger limitador de coincidencias por busqueda, por mercadolibre
	*/
	private $limit=50;

	/**
	* @var string identificador de la pregunta en mercadolibre
	*/
	private $cuestion_id=NULL;

	/**
	* Retorna contenido de la variable $headerRequest
	*
	* @return string contenido de variable
	*/
	public function getHeaderRequest() {
		return $this->headerRequest;
	}

	/**
	* Establece el contenido del Request
	*
	* @param string $a contenido de variable
	*/
	public function setHeaderRequest($a=NULL) {
		$this->headerRequest= ($a ? $a:NULL);
	}

	/**
	* Retorna contenido de la variable $headerResponse
	*
	* @return string contenido de variable
	*/
	public function getHeaderResponse() {
		return $this->headerResponse;
	}

	/**
	* Establece el contenido del Response
	*
	* @param string $a contenido de variable
	*/
	public function setHeaderResponse($a=NULL) {
		$this->headerResponse= ($a ? $a:NULL);
	}

	/**
	* Establece el contenido de la variable $response
	*
	* @param string $a contenido de variable
	*/
	public function setRespuesta($a=NULL) {
		$this->response= ($a ? $a:NULL);
	}

	/**
	* Retorna el contenido de la variable $response
	*
	* @return string contenido de variable
	*/
	public function getRespuesta($a=NULL) {
		return $this->response;
	}

	/**
	* establece el nombre de usuario
	* @param string $a username
	*/
	public function setUser($a=NULL) {
		$this->user= ($a ? $a:NULL);
	}

	/**
	* retorna el nombre de usuario
	*
	* @return string username
	*/
	public function getUser($a=NULL) {
		return $this->user;
	}

	/**
	* establece el Key o ID
	*
	* @param string $a keyname
	*/
	public function setKey($a=NULL) {
		$this->key= (self::APP_ID ? self::APP_ID : ($a ? $a:NULL));
	}

	/**
	* retorna el Key o ID
	*
	* @return string keyname
	*/
	public function getKey($a=NULL) {
		return $this->key;
	}

	/**
	* establece el secreto
	*
	* @param string $a secret
	*/
	public function setSecret($a=NULL) {
		$this->secret= (self::APP_SECRET ? self::APP_SECRET : ($a ? $a:NULL));
	}

	/**
	* retorn el secreto
	*
	* @return string secret
	*/
	public function getSecret($a=NULL) {
		return $this->secret;
	}

	/**
	* establece el lenguaje o idioma de conexion
	*
	* @param string $a abreviatura lenguaje
	*/
	public function setWsLang($a=NULL) {
		if( $a ) {
			foreach($this->sitesMercado as $key=>$val ) {
				if( !strcmp($key, strtolower($a)) ) {
					$this->wsLang= $key; # establece el lenguaje
					$this->setWsAuth($val["url"]); # url del sitio de autentificaciones
					$this->apiAuthName= $val["name"]; # nombre abreviado, ejm: MLM (para mexico)
				}
			}
		}
	}

	/**
	* retorna el lenguaje o idioma de conexion
	*
	* @return string abreviatura lenguaje
	*/
	public function getWsLang($a=NULL) {
		return $this->wsLang;
	}

	/**
	* retorna el nombre del WS en base al lenguaje cargado
	*
	* @return string abreviatura nombre de lenguaje
	*/
	public function getWsLangName() {
		return $this->apiAuthName;
	}

	/**
	* retorna la base de datos de lenguajes soportada
	*
	* @return array lista de enguajes
	*/
	public function getLangDb() {
		return $this->sitesMercado;
	}

	/**
	* establece activacion del modo Sandbox (pruebas)
	*
	* @param boolean $a estado
	*/
	public function setSanbox($a=false) {
		$this->sandbox= ($a ? $a:false);
	}

	/**
	* retorna el estado del modo Sandbox
	*
	* @return boolean estado
	*/
	public function isSandbox() {
		return ($this->sandbox ? true:false);
	}

	/**
	* establece el API Webserer de la conexion
	*/
	public function setWebserviceAPI() {
		$this->apiUrl= ($this->isSandbox() ? self::API : self::API_SANDBOX );
	}

	/**
	* retorna el webservice API de la conexion
	*
	* @return string url api
	*/
	public function getWsAPI() {
		return $this->apiUrl;
	}

	/**
	* establece el webservice de autentificacion
	*/
	public function setWsAuth($a=NULL) {
		$this->apiAuth= ($a ? $a:NULL);
	}

	/**
	* retorna el webservice de Auentificacion
	*
	* @return string url api
	*/
	public function getWsAuth() {
		return $this->apiAuth;
	}

	/**
	* establece el mensaje de exito
	*
	* @param string $a mensaje
	*/
	public function setSucess($a=NULL, $c=NULL) {
		$this->sucess= ($a ? $a:NULL);
		$this->setSucessCode($c);
	}

	/**
	* establece el codio del mensaje de exito
	*
	* @param string $a codigo
	*/
	public function setSucessCode($a=NULL) {
		$this->sucess_code= ($a ? $a:NULL);
	}

	/**
	* retorna el mensaje de exito
	*
	* @return string mensaje
	*/
	public function getSucess() {
		return $this->sucess;
	}

	/**
	* establece el mensaje de error
	*
	* @param string $a mensaje
	* @param string $c codigo de error
	*/
	public function setError($a=NULL, $c=NULL) {
		$this->error= ($a ? $a:NULL);
		$this->setErrorCode($c);
	}

	/**
	* establece el codio del mensaje de error
	*
	* @param string $a codigo
	*/
	public function setErrorCode($a=NULL) {
		$this->error_code= ($a ? $a:NULL);
	}

	/**
	* retorna el mensaje de error
	*
	* @return string mensaje
	*/
	public function getError() {
		return $this->error;
	}

	/**
	* analizador de respuestas
	*/
	public function debugResponse() {
		#echo '<br><br>--- Request:<br>';
		#print_r($this->getHeaderRequest());
		#echo '<br><br>--- Response: <br>';
		#print_r($this->getHeaderResponse());

		if( !$this->getHeaderResponse() )
			$this->setError("No se recibieron datos del servidor", "008");
		else { # datos con exito
			$r= explode("\r\n\r\n", $this->getHeaderResponse(), 2);

			if( strstr($r[0], "Continue") ) { # continua, el ID ya existe
				$y= explode("\r\n\r\n", $r[1], 2);
				$this->setSucess(json_decode($y[1]), "01");
			}
			else
				$this->setSucess(json_decode($r[1]), "01");
		}
	}

	/**
	* verifica si una token ya existe
	*
	* @return boolean estado de existencia
	*/
	public function isTokenExist() {
		return ($this->token ? true:false);
	}

	/**
	* verifica si una token_refresh ya existe
	*
	* @return boolean estado de existencia
	*/
	public function isTokenRefreshExist() {
		return ($this->token_refresh ? true:false);
	}

	/**
	* genera una token de conexion (refresqueo)
	*/
	public function createToken() {
		# if( !$this->getUser() ) 	$this->setError("no indico un username", "001");
		if( !$this->getSecret() ) 	$this->setError("no indico el secreto para la conexion", "002");
		else if( !$this->getKey() ) 	$this->setError("no indico el Key de la conexion", "003");
		else if( !$this->getWsLang() ) 	$this->setError("no indico el lenguaje o pais de su conexion", "004");
		else if( !$this->getWsAPI() ) 	$this->setError("no indico el webservice de mercado libre", "005");
		else if( !$this->isTokenExist() ) 	$this->setError("no existe token anterior para renovar", "010");
		else if( !$this->isTokenRefreshExist() ) 	$this->setError("no existe token de refresqueo anterior para renovar", "011");
		else {
			$url= '/oauth/token?';
			$param= array(
				"grant_type"=>"refresh_token", 
				"client_id"=>$this->getKey(),  
				"client_secret"=>$this->getSecret(), 
				"refresh_token"=>$this->getTokenRefresh()
			);

			$this->sendToMercado($url.$this->serialization($param), json_encode($param));
			$r= $this->getSucess();

			if( !$r )
				$this->setError("No se recibieron datos del servidor", "008");
			else {
				$this->setError(NULL);
				$this->setRespuesta(array(
						"token_refresh"=>$r->refresh_token, 
						"token"=>$r->access_token, 
						"expira"=>(($r->expires_in)+time())
					)
				);
			}
		}
	}

	/**
	* establece una token existente o guardada en sistema
	*
	* @param string $a hash token
	* @param string $b hash token refresh **opcional**
	*/
	public function setToken($a=NULL, $b=NULL) {
		$this->token= ($a ? $a:NULL);
		$this->token_refresh= ($b ? $b:NULL);
	}

	/**
	* retorna o crea una token
	*
	* @return string hash token
	*/
	public function getToken() {
		return $this->token;
	}

	/**
	* retorna o crea una token refresh
	*
	* @return string hash token refresh
	*/
	public function getTokenRefresh() {
		return $this->token_refresh;
	}

	/**
	* enviar la informacion a mercado libre
	*
	* @param string $onGet datos adicionales para el URL en GET
	* @param string $postData datos en json para el flujo POST
	* @param string $metodo flujo de datos para usarse con: PUT, DELETE y UPDATE
	*/
	public function sendToMercado($onGet=NULL, $postData=NULL, $metodo=NULL) {
		$headers= array("Content-Type: application/json");
		$dataGet= ($onGet ? $onGet:'');
		$dataSend= ($postData ? $postData:NULL);

		$s= curl_init();
		curl_setopt($s, CURLOPT_URL, $this->getWsAPI().$dataGet );
		curl_setopt($s, CURLOPT_SSL_VERIFYPEER, true );
		curl_setopt($s, CURLOPT_USERAGENT, 'moneyBox-ERP' );
		curl_setopt($s, CURLOPT_HTTPHEADER, $headers );
		curl_setopt($s, CURLOPT_HEADER, 1 );
		
		if( $metodo ) 	curl_setopt($s, CURLOPT_CUSTOMREQUEST, $metodo );
		if( $dataSend ) {
			curl_setopt($s, CURLOPT_POST, 1);
			curl_setopt($s, CURLOPT_POSTFIELDS, $dataSend);
		}

		curl_setopt($s, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($s, CURLOPT_VERBOSE, TRUE);
		curl_setopt($s, CURLINFO_HEADER_OUT, true);
		$resp= curl_exec($s);
		$rq= curl_getinfo($s);
		$this->setHeaderRequest($rq["request_header"]); // request
		$this->setHeaderResponse($resp); // response
		$this->debugResponse();
		curl_close($s);
		unset($rq, $resp, $s);
	}

	/**
	* proporciona enlace de login a tu App
	*
	* @return string url
	*/
	public function getLoginUrl() {
		# if( !$this->getUser() ) 	$this->setError("no indico un username", "001");
		if( !$this->getSecret() ) 	$this->setError("no indico el secreto para la conexion", "002");
		else if( !$this->getKey() ) 	$this->setError("no indico el Key de la conexion", "003");
		else if( !$this->getWsLang() ) 	$this->setError("no indico el lenguaje o pais de su conexion", "004");
		else if( !$this->getWsAPI() ) 	$this->setError("no indico el webservice de mercado libre", "005");
		else {
			return $this->getWsAuth().'/authorization?response_type=code&client_id='.$this->getKey();
		}
	}

	/**
	* envia la autorizacion con el codigo para recibir finalmente el access_token
	*
	* @param string $code el codigo de autorizacion
	* @param string $redirect direccion web de redireccionamiento
	*/
	public function getAutorization($code=NULL, $redirect=NULL) {
		# if( !$this->getUser() ) 	$this->setError("no indico un username", "001");
		if( !$this->getSecret() ) 	$this->setError("no indico el secreto para la conexion", "002");
		else if( !$this->getKey() ) 	$this->setError("no indico el Key de la conexion", "003");
		else if( !$this->getWsLang() ) 	$this->setError("no indico el lenguaje o pais de su conexion", "004");
		else if( !$this->getWsAPI() ) 	$this->setError("no indico el webservice de mercado libre", "005");
		else if( !$code ) 	$this->setError("no se indico el codigo de autorizacion", "007");
		else {
			$url= '/oauth/token?';
			$info= array(
				"grant_type"=>"authorization_code", 
				"client_id"=>$this->getKey(), 
				"client_secret"=>$this->getSecret(), 
				"code"=>$code, 
				"redirect_uri"=>(self::URL_REDIRECT ? self::URL_REDIRECT : $redirect)
			);

			$this->sendToMercado($url.$this->serialization($info), json_encode($info));
			$r= $this->getSucess(); # json data

			if( !$r )
				$this->setError("No se recibieron datos del servidor", "008");
			else {
				$this->setError(NULL);
				$this->setRespuesta( array(
					"token_refresh"=>$r->refresh_token, 
					"token"=>$r->access_token, 
					"expira"=>(($r->expires_in)+time())
					)
				);
			}
			unset($url);
		}
	}

	/**
	* serializa datos para envio por GET
	*
	* @param array $a informacion en arreglo
	* @return string datos serializados
	*/
	public function serialization($a=NULL) {
		if( is_array($a) && count($a) ) {
			$aux='';
			foreach( $a as $key=>$val )
				$aux .= ($aux ? '&':''). $key. '='. $val;
			return $aux;
		}
	return 0;
	}

	/**
	* retorna la informacion de tu perfil de usuario
	*/
	public function getMyInfo() {
		if( !$this->getSecret() ) 	$this->setError("no indico el secreto para la conexion", "002");
		else if( !$this->getKey() ) 	$this->setError("no indico el Key de la conexion", "003");
		else if( !$this->getWsLang() ) 	$this->setError("no indico el lenguaje o pais de su conexion", "004");
		else if( !$this->getWsAPI() ) 	$this->setError("no indico el webservice de mercado libre", "005");
		else if( !$this->isTokenExist() ) 	$this->setError("no existe una token para conexion", "009");
		else {
			$url= '/users/me?';
			$info= array("access_token"=>$this->getToken());
			$this->sendToMercado($url.$this->serialization($info), null);
			$r= $this->getSucess();

			if( !$r )
				$this->setError("No se recibieron datos del servidor", "008");
			else {
				$this->setError(NULL);
				$this->setRespuesta( array(
					"id"=>$r->id, 
					"user_id"=>$r->nickname, 
					"status"=>$r->status, 
					"email"=>$r->email, 
					"link"=>$r->permalink
					)
				);
			}
		}
	}

	/**
	* obtiene lista de condiciones o la condicion coincidente
	*
	* @param string identificador de la condicion necesaria
	* @return string/array si requiere lista retorna array, caso contrario la condicion coincidente
	*/
	public function getCondiciones($c=NULL) {
		$param= array("1"=>"new", "2"=>"used", "3"=>"used");

		if( is_numeric($c) ) {
			foreach( $param as $key=>$val ) {
				if( $key==$c ) 	$r= $val;
			}
		}
		else {
			$r= $param;
		}
	return $r;
	}

	/**
	* establecemos numero del item existente en mercado
	*
	* @param string numero o identificador del item
	*/
	public function setItemId($a=NULL) {
		$this->item= ($a ? $a:'');
	}

	/**
	* retorna numero del item existente en mercado
	*
	* @return string numero o identificador del item
	*/
	public function getItemId($a=NULL) {
		return $this->item;
	}

	/**
	* publica o crea un nuevo item
	*
	* @param array datos del producto
	*/
	public function addItem($dataPost=NULL) {
		# if( !$this->getUser() ) 	$this->setError("no indico un username", "001");
		if( !$this->getSecret() ) 	$this->setError("no indico el secreto para la conexion", "002");
		else if( !$this->getKey() ) 	$this->setError("no indico el Key de la conexion", "003");
		else if( !$this->getWsLang() ) 	$this->setError("no indico el lenguaje o pais de su conexion", "004");
		else if( !$this->getWsAPI() ) 	$this->setError("no indico el webservice de mercado libre", "005");
		else if( !$this->isTokenExist() ) 	$this->setError("no existe token anterior para renovar", "010");
		else if( !is_array($dataPost) && !count($dataPost) ) 	$this->setError("no indico la informacion del producto", "012");
		else {
			$url= '/items'. ($this->getItemId() ? '/'.$this->getItemId():''). '?access_token='. $this->getToken();
			$this->sendToMercado($url, json_encode($dataPost), ($this->getItemId() ? 'PUT':false));
			$r= $this->getSucess();

			if( !$r)
				$this->setError("No se recibieron datos del servidor", "008");
			else if( isset($r->error) )
				$this->setError($r->error.($r->cause ? " - ".$r->cause:""), "999");
			else {
				$this->setError(NULL);
				$this->setRespuesta($r);
			}
		}
	}

	/**
	* modifica un item existente
	*
	* @param array $dataPost descripcion del item en base al requerimiento de mercadolibre
	*/
	public function modItem($dataPost=NULL) {
		# if( !$this->getUser() ) 	$this->setError("no indico un username", "001");
		if( !$this->getSecret() ) 	$this->setError("no indico el secreto para la conexion", "002");
		else if( !$this->getKey() ) 	$this->setError("no indico el Key de la conexion", "003");
		else if( !$this->getWsLang() ) 	$this->setError("no indico el lenguaje o pais de su conexion", "004");
		else if( !$this->getWsAPI() ) 	$this->setError("no indico el webservice de mercado libre", "005");
		else if( !$this->isTokenExist() ) 	$this->setError("no existe token anterior para renovar", "010");
		else if( !$this->getItemId() ) 	$this->setError("no indico el identificador del producto", "013");
		else if( !is_array($dataPost) && !count($dataPost) ) 	$this->setError("no indico la informacion del producto", "012");
		else {
			$url= '/items/'.$this->getItemId(). '?access_token='. $this->getToken();
			$this->sendToMercado($url, json_encode($dataPost), ($this->getItemId() ? 'PUT':false));
			$r= $this->getSucess();

			if( !$r)
				$this->setError("No se recibieron datos del servidor", "008");
			else if( isset($r->error) )
				$this->setError($r->error, "999");
			else {
				$this->setError(NULL);
				$this->setRespuesta($r);
			}
		}
	}

	/**
	* detiene publicacion del item existente
	*/
	public function stopItem() {
		# if( !$this->getUser() ) 	$this->setError("no indico un username", "001");
		if( !$this->getSecret() ) 	$this->setError("no indico el secreto para la conexion", "002");
		else if( !$this->getKey() ) 	$this->setError("no indico el Key de la conexion", "003");
		else if( !$this->getWsLang() ) 	$this->setError("no indico el lenguaje o pais de su conexion", "004");
		else if( !$this->getWsAPI() ) 	$this->setError("no indico el webservice de mercado libre", "005");
		else if( !$this->isTokenExist() ) 	$this->setError("no existe token anterior para renovar", "010");
		else if( !$this->getItemId() ) 	$this->setError("no indico el identificador del producto", "013");
		else {
			$dataPost= array("status"=>"paused");
			$url= '/items/'.$this->getItemId(). '?access_token='. $this->getToken();
			$this->sendToMercado($url, json_encode($dataPost), ($this->getItemId() ? 'PUT':false));
			$r= $this->getSucess();

			if( !$r)
				$this->setError("No se recibieron datos del servidor", "008");
			else if( isset($r->error) )
				$this->setError($r->error, "999");
			else {
				$this->setError(NULL);
				$this->setRespuesta($r);
			}
		}
	}


	/**
	* cierra publicacion del item existente
	*/
	public function closeItem() {
		# if( !$this->getUser() ) 	$this->setError("no indico un username", "001");
		if( !$this->getSecret() ) 	$this->setError("no indico el secreto para la conexion", "002");
		else if( !$this->getKey() ) 	$this->setError("no indico el Key de la conexion", "003");
		else if( !$this->getWsLang() ) 	$this->setError("no indico el lenguaje o pais de su conexion", "004");
		else if( !$this->getWsAPI() ) 	$this->setError("no indico el webservice de mercado libre", "005");
		else if( !$this->isTokenExist() ) 	$this->setError("no existe token anterior para renovar", "010");
		else if( !$this->getItemId() ) 	$this->setError("no indico el identificador del producto", "013");
		else {
			$dataPost= array("status"=>"closed");
			$url= '/items/'.$this->getItemId(). '?access_token='. $this->getToken();
			$this->sendToMercado($url, json_encode($dataPost), ($this->getItemId() ? 'PUT':false));
			$r= $this->getSucess();

			if( !$r)
				$this->setError("No se recibieron datos del servidor", "008");
			else if( isset($r->error) )
				$this->setError($r->error, "999");
			else {
				$this->setError(NULL);
				$this->setRespuesta($r);
			}
		}
	}

	/**
	* elimina un item existente
	*/
	public function delItem() {
		# if( !$this->getUser() ) 	$this->setError("no indico un username", "001");
		if( !$this->getSecret() ) 	$this->setError("no indico el secreto para la conexion", "002");
		else if( !$this->getKey() ) 	$this->setError("no indico el Key de la conexion", "003");
		else if( !$this->getWsLang() ) 	$this->setError("no indico el lenguaje o pais de su conexion", "004");
		else if( !$this->getWsAPI() ) 	$this->setError("no indico el webservice de mercado libre", "005");
		else if( !$this->isTokenExist() ) 	$this->setError("no existe token anterior para renovar", "010");
		else if( !$this->getItemId() ) 	$this->setError("no indico el identificador del producto", "013");
		else {
			$dataSend= array( "deleted"=>true );
			$url= '/items/'.$this->getItemId(). '?access_token='. $this->getToken();
			$this->sendToMercado($url, json_encode($dataSend), ($this->getItemId() ? 'PUT':false));
			$r= $this->getSucess();

			if( !$r)
				$this->setError("No se recibieron datos del servidor", "008");
			else if( isset($r->error) )
				$this->setError($r->error, "999");
			else {
				$this->setError(NULL);
				$this->setRespuesta($r);
			}
		}
	}

	/**
	* retorna el limitador establecido por mercadolibre
	*
	* @return intiger numero del limite
	*/
	public function getLimiter() {
		return $this->limit;
	}

	/**
	* establece numer de pagina para el paginador
	*
	* @param intiger $a numero de la pagina
	*/
	public function setPagina($a=NULL) {
		$this->pagina= ($a ? $a:0);
	}

	/**
	* retorna el numero de pagina guardado
	*
	* @return intiger numero de pagina
	*/
	public function getPagina() {
		return $this->pagina;
	}

	/**
	* obtiene lista de items o informacion de un item en particular
	*
	* @param string $item es el identificador del item, opcional..
	*/
	public function getItem($item=NULL) {
		if( !$this->getUser() ) 	$this->setError("no indico un username", "001");
		else if( !$this->getSecret() ) 	$this->setError("no indico el secreto para la conexion", "002");
		else if( !$this->getKey() ) 	$this->setError("no indico el Key de la conexion", "003");
		else if( !$this->getWsLang() ) 	$this->setError("no indico el lenguaje o pais de su conexion", "004");
		else if( !$this->getWsAPI() ) 	$this->setError("no indico el webservice de mercado libre", "005");
		else if( !$this->isTokenExist() ) 	$this->setError("no existe token anterior para renovar", "010");
		else {
			$idItem= ($item ? $item:($this->getItemId() ? $this->getItemId():NULL));
			$url= '/users/'. $this->getUser(). '/items/search?offset='. ($this->getPagina() ? (($this->getPagina()*$this->getLimiter())-$this->getLimiter()):$this->getPagina()). '&limit='. $this->getLimiter(). '&access_token='. $this->getToken();

			$this->sendToMercado($url, NULL);
			$r= $this->getSucess();

			if( !$r)
				$this->setError("No se recibieron datos del servidor", "008");
			else if( isset($r->error) )
				$this->setError($r->error, "999");
			else {
				$this->setError(NULL);
				$this->setRespuesta($r);
			}
		}
	}

	/**
	* obtiene los catalogosy subcatalogos
	*
	* @return array lista de catalogos o subcatalogos
	*/
	public function getCatalogos($thisCat=NULL) {
		if( !$this->getSecret() ) 	$this->setError("no indico el secreto para la conexion", "002");
		else if( !$this->getKey() ) 	$this->setError("no indico el Key de la conexion", "003");
		else if( !$this->getWsLang() ) 	$this->setError("no indico el lenguaje o pais de su conexion", "004");
		else if( !$this->getWsAPI() ) 	$this->setError("no indico el webservice de mercado libre", "005");
		else if( !$this->isTokenExist() ) 	$this->setError("no existe token anterior para renovar", "010");
		else {
			if( $thisCat ) # hay categoria especifica
				$url= '/categories/'. $thisCat. '?access_token='. $this->getToken();
			else 
				$url= '/sites/'.$this->getWsLangName(). '/categories?access_token='. $this->getToken();

			$this->sendToMercado($url, NULL);
			$r= $this->getSucess();

			if( !$r)
				$this->setError("No se recibieron datos del servidor", "008");
			else if( isset($r->error) )
				$this->setError($r->error, "999");
			else {
				$this->setError(NULL);
				$this->setRespuesta($r);
			}
		}
	}

	/**
	* busca y obtiene las preguntas de un item especifico
	*/
	public function getCuestions() {
		if( !$this->getSecret() ) 	$this->setError("no indico el secreto para la conexion", "002");
		else if( !$this->getKey() ) 	$this->setError("no indico el Key de la conexion", "003");
		else if( !$this->getWsLang() ) 	$this->setError("no indico el lenguaje o pais de su conexion", "004");
		else if( !$this->getWsAPI() ) 	$this->setError("no indico el webservice de mercado libre", "005");
		else if( !$this->isTokenExist() ) 	$this->setError("no existe token anterior para renovar", "010");
		else if( !$this->getItemId() ) 	$this->setError("no indico el identificador del producto", "013");
		else {
			$url= '/questions/search?search_type=scan&item='.$this->getItemId(). '&access_token='. $this->getToken();
			$this->sendToMercado($url, NULL, NULL);
			$r= $this->getSucess();

			if( !$r)
				$this->setError("No se recibieron datos del servidor", "008");
			else if( isset($r->error) )
				$this->setError($r->error, "999");
			else {
				$this->setError(NULL);
				$this->setRespuesta($r);
			}
		}
	}

	/**
	* establece el identificador de la pregunta a responder
	*
	* @param string $a identificador de la pregunta
	*/
	public function setCuestionId($a=NULL) {
		$this->cuestion_id= ($a ? $a:NULL);
	}

	/**
	* Retorna el identificador de la pregunta
	*
	* @return string identificador de mercadolibre
	*/
	public function getCuestionId() {
		return $this->cuestion_id;
	}

	/**
	* responder a preguntas
	*/
	public function setCuestionResponse($msg=NULL) {
		if( !$this->getSecret() ) 	$this->setError("no indico el secreto para la conexion", "002");
		else if( !$this->getKey() ) 	$this->setError("no indico el Key de la conexion", "003");
		else if( !$this->getWsLang() ) 	$this->setError("no indico el lenguaje o pais de su conexion", "004");
		else if( !$this->getWsAPI() ) 	$this->setError("no indico el webservice de mercado libre", "005");
		else if( !$this->isTokenExist() ) 	$this->setError("no existe token anterior para renovar", "010");
		else if( !$this->getItemId() ) 	$this->setError("no indico el identificador del producto", "013");
		else if( !$this->getCuestionId() ) 	$this->setError("no indico el identificador de la pregunta", "015");
		else if( !$msg || is_array($msg) ) 	$this->setError("no indico el mensaje de la respuesta para la pregunta", "014");
		else {
			$url= '/answers?access_token='. $this->getToken();
			$data= array(
				"question_id"=>$this->getCuestionId(), 
				"text"=>$msg
			);

			$this->sendToMercado($url, json_encode($data), NULL);
			$r= $this->getSucess();

			if( !$r)
				$this->setError("No se recibieron datos del servidor", "008");
			else if( isset($r->error) )
				$this->setError($r->error, "999");
			else {
				$this->setError(NULL);
				$this->setRespuesta($r);
			}
		}
	}

	/**
	* constructor de la clase Mercadolibre
	*/
	public function __construct($key=NULL, $secret=NULL, $siteLang=NULL, $sandbox=false) {
		$this->setSanbox($sandbox);
		# $this->setUser($user);
		$this->setKey($key);
		$this->setSecret($secret);
		$this->setWsLang($siteLang);
		$this->setWebserviceAPI();
	}
}
?>