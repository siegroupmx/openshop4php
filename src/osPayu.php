<?php
/**
* OpenShop4PHP - edicion PayU
*
* libreria para conectar al servicio de cobro PayU
*
* @version 1.3-stable
* @author Angel Haniel Cantu Jauregui
* @author angel.cantu@sie-group.net
* @author https://www.moneybox.business
* 
*/

class PayU {
	/**
	* @var string URL del API Payu
	*/
	const API='https://api.payulatam.com';

	/**
	* @var string URL del API Sanbox de Payu
	*/
	const API_SANDBOX='https://sandbox.api.payulatam.com';

	/**
	* @var string URL de Servicios de Payu
	*/
	const API_SERVICE='/payments-api/4.0/service.cgi';

	/**
	* @var string URL del Reportes de Payu
	*/
	const API_REPORTS='/reports-api/4.0/service.cgi';

	/**
	* @var string URL del Sus de Payu
	*/
	const API_SUS='/payments-api/rest/4.3/';

	/**
	* @var string APP Userq ue puede asignarse por funcion o manualmente
	*/
	const APP_USER='';

	/**
	* @var string Sanbox APP Userq ue puede asignarse por funcion o manualmente
	*/
	const SANDBOX_APP_USER='pRRXKOl8ikMmt9u';

	/**
	* @var string APP Key que puede asignarse por funcion o manualmente
	*/
	const APP_KEY='';

	/**
	* @var string Sandbox APP Key que puede asignarse por funcion o manualmente
	*/
	const SANDBOX_APP_KEY='4Vj8eK4rloUd272L48hsrarnUA';

	/**
	* @var string APP Id que puede asignarse por funcion o manualmente
	*/
	const APP_ID='';

	/**
	* @var string Sandbox APP Id que puede asignarse por funcion o manualmente
	*/
	const SANDBOX_APP_ID='512324';

	/**
	* @var string APP MerchId que puede asignarse por funcion o manualmente
	*/
	const APP_MERCHID='';

	/**
	* @var string Sandbox APP MerchId que puede asignarse por funcion o manualmente
	*/
	const SANDBOX_APP_MERCHID='508029';

	/**
	* @var string guarda el username o ID usuario
	*/
	private $user=NULL;

	/**
	* @var string codigo del APP ID
	*/
	private $key=NULL;

	/**
	* @var string codigo del APP ID
	*/
	private $id=NULL;

	/**
	* @var string codigo del APP ID
	*/
	private $merchId=NULL;

	/**
	* @var string URL del API a consultar
	*/
	private $apiUrl=NULL;

	/**
	* @var boolean valor para indicar si la Sandbox esta habilitada o deshabilitada
	*/
	private $sandbox=false;

	/**
	* @var string mensaje de exito de la ultima transaccion
	*/
	private $sucess=NULL;

	/**
	* @var string codigo del mensaje de exito de la ultima transaccion
	*/
	private $sucess_code=NULL;

	/**
	* @var string mensaje de error de la ultima transaccion
	*/
	private $error=NULL;

	/**
	* @var string codigo del mensaje de error de la ultima transaccion
	*/
	private $error_code=NULL;

	/**
	* @var string mensaje completo del Request enviado
	*/
	private $headerRequest= NULL;

	/**
	* @var string mensaje completo del Response recibido
	*/
	private $headerResponse=NULL;

	/**
	* @var string datos especificamente formados como respuesta final a una consulta exitosa, tambien puede ser un array
	*/
	private $response=NULL;

	/**
	* @var string tipo de moneda para divisa
	*/
	private $moneda=NULL;

	/**
	* @var string identificador de la orden
	*/
	private $orderId=NULL;

	/**
	* @var string identificador de la transaccion
	*/
	private $transactionId=NULL;

	/**
	* @var string estado de la transaccion
	*/
	private $transactionState=NULL;

	/**
	* @var string abreviatura de pais seleccionada
	*/
	private $country=NULL;

	/**
	* @var boolean establece si el metodo de pago es de tarjeta (true) o no (false)
	*/
	private $isCard=false;

	/**
	* @var array paises disponibles
	*/
	private $countryDb= array(
		"ar"=>"argentina", 
		"br"=>"brasil", 
		"co"=>"colombia", 
		"mx"=>"mexico"
	);

	/**
	* @var array lista de metodos de pago soportados
	*/
	private $paymentList=array();

	/**
	* @var intiger identificador del metodo de pago detectado
	*/
	private $paymentTypeId=NULL;

	/**
	* @var string nombre del metodo de pago detectado
	*/
	private $paymentTypeName=NULL;

	/**
	* @var string ruta donde guardaremos los recibos, boucher y lineas de captura
	*/
	private $pathRecibo=NULL;

	/**
	* @var string nombre del recibo, boucher o linea de captura a guardar
	*/
	private $reciboName=NULL;

	/**
	* @var string codigo de barras de la linea de captura en pagos en efectivo
	*/
	private $barcode=NULL;

	/**
	* @var string codigo de transferencia del codigo de una transferencia resultante
	*/
	private $transferCode=NULL;

	/**
	* @var array arreglo de estados de una transaccion con PayU
	*/
	private $transactionStatusDb= array(
		"APPROVED"=>1,
		"PENDING"=>2,  
		"REJECTED"=>3, 
		"DECLINED"=>4,  
		"EXPIRED"=>5, 
		"ERROR"=>6, 
		"SUBMITED"=>7
	);
	
	/**
	* @var array arreglo de estados de una transaccion en latinoamericano
	*/
	private $transactionStatusTrans= array(
		"es"=>array(
			"APPROVED"=>array("name"=>"Aprobado", "msg"=>"Transacci&oacute;n aprobada"),
			"PENDING"=>array("name"=>"Pendiente", "msg"=>"Transacci&oacute;n pendiente"),
			"REJECTED"=>array("name"=>"Rechazado", "msg"=>"Transacci&oacute;n rechazada"),
			"DECLINED"=>array("name"=>"Declinado", "msg"=>"Transacci&oacute;n declinada"),
			"EXPIRED"=>array("name"=>"Expiro", "msg"=>"Transacci&oacute;n expiro"),
			"ERROR"=>array("name"=>"Con Errores", "msg"=>"Error al procesarse la transacci&oacute;n"),
			"SUBMITED"=>array("name"=>"Zombie", "msg"=>"Transacci&oacute;n no fue respondida, algo sucedio"), 
			"PENDING_TRANSACTION_CONFIRMATION"=>array("name"=>"Pendiente de Confirmacion", "msg"=>"Transacci&oacute;n pendiente de ser confirmada")
		)
	);


	/**
	* @var array arreglo de recursos de imagenes a metodos de pago en efectivo
	*/
	private $resourcesPayment= array(
		"OXXO"=>"/app/modulos/openshop4php/resources/payu/oxxo.jpg", 
		"SEVEN_ELEVEN"=>"/app/modulos/openshop4php/resources/payu/7eleven.jpg", 
		"BALOTO"=>"/app/modulos/openshop4php/resources/payu/baloto.jpg", 
		"EFECTY"=>"/app/modulos/openshop4php/resources/payu/efecty.jpg", 
		"PSE"=>"/app/modulos/openshop4php/resources/payu/pse.jpg"
	);

	/**
	* @var array arreglo de recursos de imagenes a metodos de pago en efectivo
	*/
	private $needPasarela= array(
		"visa", "mastercard", "debito", "credito", 
		"pse"
	);

	/**
	* @var string estado de una transaccion PayU para Tarjeta o Efectivo
	*/
	private $transactionStatus=NULL;

	/**
	* @var string estado de una transaccion PayU en NOMBRE para Tarjeta o Efectivo
	*/
	private $transactionStatusName=NULL;

	/**
	* @var string ruta al HTML para pago con Tarjeta
	*/
	private $formatoTarjeta= '/app/modulos/openshop4php/resources/payu/tarjeta.html';

	/**
	* @var string ruta al HTML para pago con Efectivo
	*/
	private $formatoEfectivo= '/app/modulos/openshop4php/resources/payu/efectivo.html';

	/**
	* @var array datos del receptor
	*/
	private $cliente=array();

	/**
	* @var array datos del cobro
	*/
	private $cobro=array();

	/**
	* @var array datos del emisor
	*/
	private $empresa=array();

	/**
	* @var string tiempo de expiracion para lineas de cobro
	*/
	private $expiracion=0;

	/**
	* @var string tiempo de reintentos para procesas el pago
	*/
	private $retry=0;

	/**
	* @var string fecha
	*/
	private $fecha=NULL;

	/**
	* @var boolean variable indica PSE activo o inactivo
	*/
	private $isPSE=false;

	/**
	* @var array arreglo para transacciones PSE
	*/
	private $pseForm=array();

	/**
	* Retorna true o false si existe PSE metodo activo
	*
	* @return bool falso o verdadeo
	*/
	public function isPSE() {
		return $this->isPSE;
	}

	/**
	* Establece la carga de datos y habilitacion del metodo PSE
	*
	*/
	public function setPSE($a=NULL) {
		if( strcmp(strtolower($this->getPaymentName()), "pse") ) {
			$this->setError("no tiene el metodo PSE cargado", "032");
			$this->setSucess(NULL);
		}
		else if( !$a || !is_array($a) || !count($a) ) {
			$this->setError("no se indico el arreglo de datos para metodo PSE", "033");
			$this->setSucess(NULL);
		}
		else {
			$this->isPSE= true;
			$this->pseForm= $a;

			$this->setError(NULL);
			$this->setSucess("metodo PSE y formulario cargados con exito");
		}
	}

	/**
	* Retorna el arreglo de PSE para enviar a PayU
	* 
	* @return array falso o array en exito
	*
	*/
	public function getPSEForm() {
		return (!$this->isPSE() ? false : $this->pseForm);
	}

	/**
	* Retorna true o false para caso que requiera pasarela para captura de datos
	*
	* @return bool falso o verdadeo
	*/
	public function needPaymentPasarel() {
		if( $this->isCard() ) # si es con tarjeta
			return true;
		else if( $this->getPaymentName() ) {
			$r= false;
			foreach( $this->needPasarela as $key ) {
				if( !strcmp(strtolower($key), strtolower($this->getPaymentName())) )
					$r= true;
			}
			return $r;
		}
		return false;
	}

	/**
	* Retorna el nombre del medio de pago
	*
	* @return string nombre del medio de pago
	*/
	public function getPaymentName() {
		return $this->paymentTypeName;
	}

	/**
	* Establece la fecha del tramite
	*
	* @param string $a la cantidad en dias
	*/
	public function setFecha($a=NULL) {
		$this->fecha= ($a ? $a:time());
	}

	/**
	* Retorna la fecha del tramite
	*
	* @return string la cantidad en dias
	*/
	public function getFecha() {
		return $this->fecha;
	}

	/**
	* Establece el tiempo de expiracion para las lineas de cobro
	*
	* @param string $a la cantidad en dias
	*/
	public function setExpirationTime($a=NULL) {
		$this->expiracion= ($a ? $a:3); # por defecto 3 dias
	}

	/**
	* Retorna el tiempo de expiracion para las lineas de cobro
	*
	* @return string la cantidad en dias
	*/
	public function getExpirationTime() {
		return ($this->getFecha()+($this->expiracion*86400));
	}

	/**
	* Establece los reintentos establecidos para hacer el pago
	*
	* @param string $a la cantidad en dias
	*/
	public function setRetryTime($a=NULL) {
		$this->retry= ($a ? $a:3);
	}

	/**
	* Retorna los reintentos establecidos para hacer el pago
	*
	* @return string la cantidad en dias
	*/
	public function getRetryTime() {
		return $this->retry;
	}

	public function getTransactionStatusTranslation($a=NULL) {
		if( !$a )	return 0;
		else {
			$idioma='es'; # de momento solo espanol
			$r=array();
			$fl=true;

			foreach( $this->transactionStatusTrans[$idioma] as $key=>$val ) {
				if( $fl ) {
					if( !strcmp(strtolower($key), strtolower($a)) ) {
						$r= $val; # asociamos
						$fl=false; # apagamos
					}
				}
			}
			return $r;
		}
	}

	/**
	* Establece el estado de una transaccion en formato Codigo
	*
	* @param string $a el texto del estado de transaccion con PayU
	*/
	public function setTransactionStatus($a=NULL) {
		$this->transactionStatus= ($transactionStatusDb[$a] ? $transactionStatusDb[$a]:NULL);
	}

	/**
	* retorna el estado de una transaccion en formato Codigo
	*
	* @return string el codigo del estado de transaccion con PayU
	*/
	public function getTransactionStatus() {
		return $this->transactionStatus;
	}

	/**
	* Establece el estado de una transaccion en formato Nombre
	*
	* @param string $a el texto del estado de transaccion con PayU
	*/
	public function setTransactionStatusName($a=NULL) {
		$this->transactionStatusName= ($transactionStatusDb[$a] ? $a:NULL);
	}

	/**
	* retorna el estado de una transaccion en formato Nombre
	*
	* @return string el texto del estado de transaccion con PayU
	*/
	public function getTransactionStatusName() {
		return $this->transactionStatusName;
	}

	/**
	* Establece el codigo resultante de la transferencia
	*
	* @param string $a el codigo
	*/
	public function setTransferCode($a=NULL) {
		$this->transferCode= ($a ? $a:NULL);
	}

	/**
	* Retorna el codigo resultante de la transferencia
	*
	* @return string el codigo de transferencia
	*/
	public function getTransferCode() {
		return $this->transferCode;
	}

	/**
	* Establece el codigo de barras obtenido
	*
	* @param string $a el codigo
	*/
	public function setBarcode($a=NULL) {
		$this->barcode= ($a ? $a:NULL);
	}

	/**
	* Retorna el codigo de barras
	*
	* @return string el codigo de barras
	*/
	public function getBarcode() {
		return $this->barcode;
	}

	/**
	* Retorna contenido de la variable $headerRequest
	*
	* @return string contenido de variable
	*/
	public function getHeaderRequest() {
		return $this->headerRequest;
	}

	/**
	* Establece el contenido del Request
	*
	* @param string $a contenido de variable
	*/
	public function setHeaderRequest($a=NULL) {
		$this->headerRequest= ($a ? $a:NULL);
	}

	/**
	* Retorna contenido de la variable $headerResponse
	*
	* @return string contenido de variable
	*/
	public function getHeaderResponse() {
		return $this->headerResponse;
	}

	/**
	* Establece el contenido del Response
	*
	* @param string $a contenido de variable
	*/
	public function setHeaderResponse($a=NULL) {
		$this->headerResponse= ($a ? $a:NULL);
	}

	/**
	* Establece el contenido de la variable $response
	*
	* @param string $a contenido de variable
	*/
	public function setRespuesta($a=NULL) {
		$this->response= ($a ? $a:NULL);
	}

	/**
	* Retorna el contenido de la variable $response
	*
	* @return string contenido de variable
	*/
	public function getRespuesta($a=NULL) {
		return $this->response;
	}

	/**
	* establece el nombre de usuario
	* @param string $a username
	*/
	public function setUser($a=NULL) {
		$this->user= ($a ? $a:($this->isSandbox() ? self::SANDBOX_APP_USER : self::APP_USER));
	}

	/**
	* retorna el nombre de usuario
	*
	* @return string username
	*/
	public function getUser($a=NULL) {
		return $this->user;
	}

	/**
	* establece el Key o ID
	*
	* @param string $a keyname
	*/
	public function setKey($a=NULL) {
		$this->key= ($a ? $a:($this->isSandbox() ? self::SANDBOX_APP_KEY : self::APP_KEY));
	}

	/**
	* retorna el Key o ID
	*
	* @return string keyname
	*/
	public function getKey($a=NULL) {
		return $this->key;
	}

	/**
	* establece el Id
	* @param string $a identificador
	*/
	public function setId($a=NULL) {
		$this->id= ($a ? $a:($this->isSandbox() ? self::SANDBOX_APP_ID : self::APP_ID));
	}
	
	/**
	* retorna el Id
	*
	* @return string identificador
	*/
	public function getId($a=NULL) {
		return $this->id;
	}

	/**
	* establece el merchandace Id
	* @param string $a merchandace Id
	*/
	public function setMerchId($a=NULL) {
		$this->merchId= ($a ? $a:($this->isSandbox() ? self::SANDBOX_APP_MERCHID : self::APP_MERCHID));
	}
	
	/**
	* retorna el nombre de merchandace Id
	*
	* @return string merchandace Id
	*/
	public function getMerchId($a=NULL) {
		return $this->merchId;
	}

	/**
	* establece la divisa de moneda
	*
	* @param string $a nombre de la divisa
	*/
	public function setMoneda($a=NULL) {
		$this->moneda= ($a ?$a:NULL);
	}

	/**
	* retorna la divisa de moneda
	*
	* @return string nombre de la divisa
	*/
	public function getMoneda($a=NULL) {
		return $this->moneda;
	}

	/**
	* establece el identificador de la Orden
	*
	* @param string $a numero de orden
	*/
	public function setOrderId($a=NULL) {
		if( !$this->getUser() )	$this->setError("no indico un username", "001");
		else if( !$this->getKey() )		$this->setError("no indico el Key de la conexion", "003");
		else if( !$this->getId() )		$this->setError("no indico el identificador de la cuenta", "019");
		else if( !$this->getMerchId() )		$this->setError("no indico el identificador del mercado", "018");
		else {
			$this->orderId= ($a ? $a:NULL);
		}
	}

	/**
	* retorna el identificador de la Orden
	*
	* @return string numeor de orden
	*/
	public function getOrderId() {
		return $this->orderId;
	}

	/**
	* establece el identificador de la Transaccion
	*
	* @param string $a numero de transaccion
	*/
	public function setTransactionId($a=NULL) {
		if( !$this->getUser() )	$this->setError("no indico un username", "001");
		else if( !$this->getKey() )		$this->setError("no indico el Key de la conexion", "003");
		else if( !$this->getId() )		$this->setError("no indico el identificador de la cuenta", "019");
		else if( !$this->getMerchId() )		$this->setError("no indico el identificador del mercado", "018");
		else {
			$this->transactionId= ($a ? $a:NULL);
		}
	}

	/**
	* retorna el identificador de la Transaccion
	*
	* @return string numero de transaccion
	*/
	public function getTransactionId() {
		return $this->transactionId;
	}

	/**
	* establece el estado de la Transaccion
	*
	* @param string $a resultado del estado de transaccion
	*/
	public function setTransactionState($a=NULL) {
		if( !$this->getUser() )	$this->setError("no indico un username", "001");
		else if( !$this->getKey() )		$this->setError("no indico el Key de la conexion", "003");
		else if( !$this->getId() )		$this->setError("no indico el identificador de la cuenta", "019");
		else if( !$this->getMerchId() )		$this->setError("no indico el identificador del mercado", "018");
		else {
			$this->transactionState= ($a ? $a:NULL);
		}
	}

	/**
	* retorna el Estado de la Transaccion
	*
	* @return string estado de la transaccion
	*/
	public function getTransactionState() {
		return $this->transactionState;
	}

	/**
	* establece activacion del modo Sandbox (pruebas)
	*
	* @param boolean $a estado
	*/
	public function setSanbox($a=NULL) {
		$this->sandbox= ((!$a || $a==1) ? true:false);
	}

	/**
	* retorna el estado del modo Sandbox
	*
	* @return boolean estado
	*/
	public function isSandbox() {
		return ($this->sandbox ? true:false);
	}

	/**
	* devuelve la abreviatura de pais seleccionada
	*
	* @return string abreviatura de pais
	*/
	public function getCountry() {
		return ($this->countryDb[$this->country] ? $this->country:NULL);
	}

	/**
	* establece la abreviatura de pais
	*
	* @param string $a abreviatura del pais a 2 letras
	*/
	public function setCountry($a=NULL) {
		if( !$a ) $this->country='mx'; # default mexico
		else {
			$r=NULL;
			foreach( $this->getCountryDb() as $key=>$val ) {
				if( !strcmp($key, $a) || !strcmp($val, $a) ) 	$r= $key;
			}
		$this->country= $r;
		unset($r);
		}
	}

	/**
	* devuelve la lista de paises soportados
	*
	* @return array lista de paises soportados
	*/
	public function getCountryDb() {
		return $this->countryDb;
	}

	/**
	* establece el API Webserer de la conexion
	*/
	public function setWebserviceAPI() {
		if( !$this->getUser() )	$this->setError("no indico un username", "001");
		else if( !$this->getKey() )		$this->setError("no indico el Key de la conexion", "003");
		else if( !$this->getId() )		$this->setError("no indico el identificador de la cuenta", "019");
		else if( !$this->getMerchId() )		$this->setError("no indico el identificador del mercado", "018");
		else {
			$this->apiUrl= ($this->isSandbox() ? self::API_SANDBOX : self::API );
		}
	}

	/**
	* retorna el webservice API de la conexion
	*
	* @return string url api
	*/
	public function getWsAPI() {
		return $this->apiUrl;
	}

	/**
	* establece el mensaje de exito
	*
	* @param string $a mensaje
	*/
	public function setSucess($a=NULL, $c=NULL) {
		$this->sucess= ($a ? $a:NULL);
		$this->setSucessCode($c);
	}

	/**
	* establece el codio del mensaje de exito
	*
	* @param string $a codigo
	*/
	public function setSucessCode($a=NULL) {
		$this->sucess_code= ($a ? $a:NULL);
	}

	/**
	* retorna el mensaje de exito
	*
	* @return string mensaje
	*/
	public function getSucess() {
		return $this->sucess;
	}

	/**
	* establece el mensaje de error
	*
	* @param string $a mensaje
	* @param string $c codigo de error
	*/
	public function setError($a=NULL, $c=NULL) {
		$this->error= ($a ? $a:NULL);
		$this->setErrorCode($c);
	}

	/**
	* establece el codio del mensaje de error
	*
	* @param string $a codigo
	*/
	public function setErrorCode($a=NULL) {
		$this->error_code= ($a ? $a:NULL);
	}

	/**
	* retorna el codigo de error
	*
	* @return string codigo de error
	*/
	public function getErrorCode($a=NULL) {
		return $this->error_code;
	}

	/**
	* retorna el mensaje de error
	*
	* @return string mensaje
	*/
	public function getError() {
		return ($this->getErrorCode() ? ($this->getErrorCode().': '):'').$this->error;
	}

	/**
	* analizador de respuestas
	*/
	public function debugResponse() {
		#echo "\n\n--- Request:\n";
		#print_r($this->getHeaderRequest());
		#echo "\n\n--- Response:\n";
		#print_r($this->getHeaderResponse());

		if( !$this->getHeaderResponse() ) 
			$this->setError("No se recibieron datos del servidor", "008");
		else { # datos con exito
			$a= explode("\r\n\r\n", $this->getHeaderResponse(), 2);

			if( strstr($a[0], "Continue") ) { # continua, el ID ya existe
				$y= explode("\r\n\r\n", $a[1], 2);
				$r= new SimpleXmlElement($y[1]);
			}
			else {
				$r= new SimpleXmlElement($a[1]);
			}

			if( $r->error ) {
				$this->setSucess(NULL);
				$this->setError($r->error, $r->code);
			}
			else
				$this->setSucess($r, "01");
		}
	}

	/**
	* enviar la informacion a PayU
	*
	* @param string $onGet datos adicionales para el URL en GET
	* @param string $postData datos en json para el flujo POST
	* @param string $metodo flujo de datos para usarse con: PUT, DELETE y UPDATE
	*/
	public function sendToPayu($onGet=NULL, $postData=NULL, $metodo=NULL) {
		$headers= array("Content-Type: application/json");
		$dataGet= ($onGet ? $onGet:'');
		$dataSend= ($postData ? $postData:NULL);

		$s= curl_init();
		curl_setopt($s, CURLOPT_URL, $this->getWsAPI().$dataGet );
		curl_setopt($s, CURLOPT_SSL_VERIFYPEER, true );
		curl_setopt($s, CURLOPT_USERAGENT, 'moneyBox-ERP' );
		curl_setopt($s, CURLOPT_HTTPHEADER, $headers );
		curl_setopt($s, CURLOPT_HEADER, 1 );
		
		if( $metodo ) 	curl_setopt($s, CURLOPT_CUSTOMREQUEST, $metodo );
		if( $dataSend ) {
			curl_setopt($s, CURLOPT_POST, 1);
			curl_setopt($s, CURLOPT_POSTFIELDS, $dataSend);
		}

		curl_setopt($s, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($s, CURLOPT_VERBOSE, TRUE);
		curl_setopt($s, CURLINFO_HEADER_OUT, true);
		$resp= curl_exec($s);
		$rq= curl_getinfo($s);
		$this->setHeaderRequest($rq["request_header"]); // request
		$this->setHeaderResponse($resp); // response
		$this->debugResponse();
		curl_close($s);
		unset($rq, $resp, $s);
	}

	/**
	* serializa datos para envio por GET
	*
	* @param array $a informacion en arreglo
	* @return string datos serializados
	*/
	public function serialization($a=NULL) {
		if( is_array($a) && count($a) ) {
			$aux='';
			foreach( $a as $key=>$val )
				$aux .= ($aux ? '&':''). $key. '='. $val;
			return $aux;
		}
	return 0;
	}

	/**
	* Genera device session
	*
	* @param string $a dato para la session
	*/
	public function getDeviceSession() {
		return md5(session_id().microtime()).'80200';
	}

	/**
	* Genera firma transaccional
	*/
	public function getSignature($a=NULL, $b=NULL) {
		return md5(
			$this->getKey().'~'.
			$this->getMerchId().'~'.
			$a.'~'. # referencia
			$b.'~'. # valor
			strtoupper($this->getMoneda())
		);
	}

	/**
	* obtiene las marcas de pago en Efectivo
	*/
	public function getEfectivoMedios($format="text") {
		if( !count($this->paymentList) ) # no hay lista de pagos soportados
			$this->getPaymentMethods(); # obtenemos lista existente

		if( !strcmp($format, "array") )
			$r=array();
		else if( !strcmp($format, "text") )
			$r='';
		else {
			$this->setError("no se indico un formato valido para obtener los medios de pago en efectivo soportados", "031"); # error
			$this->setSucess(NULL); # sin respuesta
		}

		foreach( $this->paymentList as $key=>$val ) {
			if( !strcmp($val->enabled, "true") ) {
				if( !strstr(strtolower($val->description), "credit") && !strstr(strtolower($val->description), "debit") && !strstr(strtolower($val->description), "visa") && !strstr(strtolower($val->description), "mastercard") && !strstr(strtolower($val->description), "amex") ) {
					if( !strcmp($format, "array") )
						$r[]= strtolower(sprintf("%s", $val->description));
					else if( !strcmp($format, "text") )
						$r .= ($r ? ', ':''). strtolower(sprintf("%s", $val->description));
				}
			}
		}

		if( !strcmp($format, "array") ) {
			$this->setError(NULL);
			$this->setRespuesta($r);
		}
		else if( !strcmp($format, "text") ) {
			$this->setError(NULL);
			$this->setRespuesta($r);
		}
		unset($r);
	}

	/**
	* obtiene el nombre del metodo de pago apartir del ID o numero
	*/
	public function getMetodoPagoPorNumero($a=NULL) {
		$r=0;
		if( !count($this->paymentList) ) # no hay lista de pagos soportados
			$this->getPaymentMethods(); # obtenemos lista existente

		if( $a ) {
			$r=0;
			foreach( $this->paymentList as $key=>$val ) {
				if( !strcmp($val->enabled, "true") ) {
					if( (!strcmp($val->id, $a) || !strcmp(strtolower($val->description), strtolower($a))) && !$r ) {
						$r= strtolower(sprintf("%s", $val->description));
					}
				}
			}
		}

		return $r;
	}

	/**
	* obtiene el ID del metodo de pago apartir del nombre
	*/
	public function getMetodoPagoPorNombre($a=NULL) {
		$r=0;
		if( !count($this->paymentList) ) # no hay lista de pagos soportados
			$this->getPaymentMethods(); # obtenemos lista existente

		if( $a ) {
			$r=0;
			foreach( $this->paymentList as $key=>$val ) {
				if( !strcmp($val->enabled, "true") ) {
					if( (!strcmp($val->id, $a) || !strcmp(strtolower($val->description), strtolower($a))) && !$r ) {
						$r= strtolower(sprintf("%s", $val->id));
					}
				}
			}
		}

		return $r;
	}

	/**
	* obtiene la informacion de una orden por su identificador
	*/
	public function getOrders() {
		if( !$this->getUser() )	$this->setError("no indico un username", "001");
		else if( !$this->getKey() )		$this->setError("no indico el Key de la conexion", "003");
		else if( !$this->getId() )		$this->setError("no indico el identificador de la cuenta", "019");
		else if( !$this->getMerchId() )		$this->setError("no indico el identificador del mercado", "018");
		else if( !$this->getOrderId() )		$this->setError("no indico el identificador de la orden", "016");
		else {
			$url= self::API_REPORTS;
			$params=array(
				"test"=>"false", 
				"language"=>"es", 
				"command"=>"ORDER_DETAIL", 
				"merchant"=>array(
					"apiLogin"=>$this->getUser(), 
					"apiKey"=>$this->getKey()
				), 
				"details"=>array(
						"orderId"=>$this->getOrderId()
				)
			);

			$this->sendToPayu($url,json_encode($params), NULL);
			$r= $this->getSucess();

			if( $r ) {
				$this->setError(NULL);
				$this->setRespuesta($r);
			}
		}
	}

	/**
	* obtiene la informacion de una transaccion
	*/
	public function getTransaction() {
		if( !$this->getUser() )	$this->setError("no indico un username", "001");
		else if( !$this->getKey() )		$this->setError("no indico el Key de la conexion", "003");
		else if( !$this->getId() )		$this->setError("no indico el identificador de la cuenta", "019");
		else if( !$this->getMerchId() )		$this->setError("no indico el identificador del mercado", "018");
		else if( !$this->getTransactionId() )		$this->setError("no indico el identificador de la transaccion", "017");
		else {
			$url= self::API_REPORTS;
			$params=array(
				"test"=>"false", 
				"language"=>"es", 
				"command"=>"TRANSACTION_RESPONSE_DETAIL", 
				"merchant"=>array(
					"apiLogin"=>$this->getUser(), 
					"apiKey"=>$this->getKey()
				), 
				"details"=>array(
						"transactionId"=>$this->getTransactionId()
				)
			);

			$this->sendToPayu($url,json_encode($params), NULL);
			$r= $this->getSucess();

			if( $r ) {
				$this->setError(NULL);
				$this->setRespuesta($r);
			}
		}
	}

	/**
	* obtiene los metodos de pago soportados para la cuenta
	*/
	public function getPaymentMethods() {
		if( !$this->getUser() )	$this->setError("no indico un username", "001");
		else if( !$this->getKey() )		$this->setError("no indico el Key de la conexion", "003");
		else if( !$this->getId() )		$this->setError("no indico el identificador de la cuenta", "019");
		else if( !$this->getMerchId() )		$this->setError("no indico el identificador del mercado", "018");
		else {
			$url= self::API_SERVICE;
			$params= array(
				"test"=>"false", 
				"language"=>"es", 
				"command"=>"GET_PAYMENT_METHODS", 
				"merchant"=>array(
					"apiLogin"=>$this->getUser(), 
					"apiKey"=>$this->getKey(), 
				)
			);

			$this->sendToPayu($url,json_encode($params), NULL);
			$r= $this->getSucess();

			if( $r ) {
				$this->setError(NULL);
				foreach( $r->paymentMethods->paymentMethodComplete as $key=>$val ) {
					if( !strcmp(strtolower($val->country), $this->getCountry()) ) {
						$aux[]= $val;
					}
				}

				$this->paymentList= $aux;
				$this->setRespuesta($aux);
			}
		}
	}

	/**
	* obtiene los bancos para pagar
	*/
	public function getBankList() {
		if( !$this->getUser() )	$this->setError("no indico un username", "001");
		else if( !$this->getKey() )		$this->setError("no indico el Key de la conexion", "003");
		else if( !$this->getId() )		$this->setError("no indico el identificador de la cuenta", "019");
		else if( !$this->getMerchId() )		$this->setError("no indico el identificador del mercado", "018");
		else {
			$url= self::API_SERVICE;
			$params= array(
				"test"=>"false", 
				"language"=>"es", 
				"command"=>"GET_BANKS_LIST", 
				"merchant"=>array(
					"apiLogin"=>$this->getUser(), 
					"apiKey"=>$this->getKey(), 
				), 
				"bankListInformation"=>array(
					"paymentMethod"=>strtoupper($this->getPaymentName()), 
					"paymentCountry"=>strtoupper($this->getCountry())
				)
			);

			$this->sendToPayu($url,json_encode($params), NULL);
			$r= $this->getSucess();

			if( $r ) {
				$this->setError(NULL);
				foreach( $r->banks->bank as $key=>$val ) {
					$aux[]= $val; # json a array
				}

				$this->setRespuesta($aux);
			}
		}
	}

	/**
	* establece el metodo de pago a utilizar
	*
	* @param string $a nombre del metodo de pago o identificador
	*/
	public function setPaymentMethod($a=NULL) {
		if( !$this->getUser() )	$this->setError("no indico un username", "001");
		else if( !$this->getKey() )		$this->setError("no indico el Key de la conexion", "003");
		else if( !$this->getId() )		$this->setError("no indico el identificador de la cuenta", "019");
		else if( !$this->getMerchId() )		$this->setError("no indico el identificador del mercado", "018");
		else {
			if( !count($this->paymentList) ) # si no fue consultado
				$this->getPaymentMethods(); # obtenemos lista existente

			foreach( $this->paymentList as $key=>$val ) {
				if( !strcmp($val->id, $a) || !strcmp(strtolower($val->description), strtolower($a)) ) {
					$this->paymentTypeId= sprintf("%s", $val->id); # obtenemos el tipo de metodo de pago
					$this->paymentTypeName= sprintf("%s", $val->description);
				}
			}

			$this->setError(NULL);
			$this->setSucess(NULL);

			if( $this->paymentTypeId && $this->paymentTypeName ) {
				$this->setSucess("Metodo establecido [". $this->paymentTypeId. "] => ".$this->paymentTypeName, "001");
				$this->setRespuesta("Metodo establecido [". $this->paymentTypeId. "] => ".$this->paymentTypeName, "001");
				$this->setCard();
			}
			else
				$this->setError("no se pudo establecer el metodo de pago indicado", "020");
		}
	}

	/**
	* Retorna los tipos de documentos soportados en PSE
	*
	* @return array tipos de documentos
	*/
	public function getPseTipoDocto() {
		return array(
			"CC"=>"Cédula de ciudadanía", 
			"CE"=>"Cédula de extranjería", 
			"NIT"=>"Número de Identificación Tributario", 
			"TI"=>"Tarjeta de Identidad", 
			"PP"=>"Pasaporte", 
			"IDC"=>"Identificador único de cliente, para el caso de ID’s únicos de clientes/usuarios de servicios públicos", 
			"CEL"=>"En caso de identificarse a través de la línea del móvil", 
			"RC"=>"Registro civil de nacimiento", 
			"DE"=>"Documento de identificación extranjero"
		);
	}

	/**
	* Retorna los tipos de regimen soportados en PSE
	*
	* @return array tipos de regimen
	*/
	public function getPseTipoRegimen() {
		return array(
			"N"=>"Persona Natural", 
			"J"=>"Persona Juridica"
		);
	}

	/**
	* Retorna el recurso de imagen del metodo de pago en efectivo
	*
	* @return string ruta a la imagen
	*/
	public function getImagePaymentMethod() {
		if( !$this->isPaymentMethodExist() )	$this->setError("no indico el nombre del metodo de pago", "021");
		else if( !$this->isCard() ) { # si no es tarjeta, retornamos logotipo
			return $this->resourcesPayment[strtoupper($this->paymentTypeName)];
		}
	}

	/**
	* verifica si el metodo de pago fue establecido
	*
	* @return boolean verdadero si las variables de Id y Name para el pago existen
	*/
	public function isPaymentMethodExist() {
		if( !$this->getUser() )	$this->setError("no indico un username", "001");
		else if( !$this->getKey() )		$this->setError("no indico el Key de la conexion", "003");
		else if( !$this->getId() )		$this->setError("no indico el identificador de la cuenta", "019");
		else if( !$this->getMerchId() )		$this->setError("no indico el identificador del mercado", "018");
		else {
			return (($this->paymentTypeName && $this->paymentTypeId) ? true:false);
		}
	}

	/**
	* Estalece si un metodo de pago seleccionado es una tarjeta
	*/
	public function setCard() {
		if( !$this->getUser() )	$this->setError("no indico un username", "001");
		else if( !$this->getKey() )		$this->setError("no indico el Key de la conexion", "003");
		else if( !$this->getId() )		$this->setError("no indico el identificador de la cuenta", "019");
		else if( !$this->getMerchId() )		$this->setError("no indico el identificador del mercado", "018");
		else if( !$this->isPaymentMethodExist() )	$this->setError("no indico el nombre del metodo de pago", "021");
		else {
			if( strstr(strtolower($this->paymentTypeName), "debit") || strstr(strtolower($this->paymentTypeName), "credit") || strstr(strtolower($this->paymentTypeName), "visa") || strstr(strtolower($this->paymentTypeName), "mastercard") || strstr(strtolower($this->paymentTypeName), "amex") )
				$this->isCard= true;
			else 	$this->isCard= false;
		}
	}

	/**
	* Retorna el estado de metodo de pago para Tarjetas
	*
	* @return boolean verdadero si es tarjeta o falso caso contrario
	*/
	public function isCard() {
		return $this->isCard;
	}

	/**
	* devuelve un template de como enviar los datos a Payu
	*
	* @param string el comando del tipo de template: efectivo o tarjeta
	*/
	public function getTemplatePayment($a=NULL) {
		if( !strcmp($a, "tarjeta") || !strcmp($a, "card") || $this->isCard() ) { # tarjeta
			return array(
				"orden_id"=>"Numero_de_Orden_De_Tu_Sistema", 
				"nombre"=>"Descripcion de la orden", 
				"notify"=>"Url_De_Las_Notificaciones", 
				"total"=>"Monto_Total_de_la_Compra", 
				"pais"=>"Pais_Del_Emisor_del_Cobro", 
				"ip"=>"IP_del_Cliente", 
				"navegador"=>"Nombre_Navegador_Cliente", 
				"cliente"=>array(
					"id"=>"Id_del_Cliente_en_Tu_Sistema", 
					"nombre"=>"Nombre_del_Cliente", 
					"email"=>"Correo_del_Cliente", 
					"telefono"=>"Numero_Telefono_del_Cliente", 
					"nit"=>"Identificacion_Fiscal_del_Cliente", 
					"calle"=>"Direccion_Calle_Cliente", 
					"colonia"=>"Direccion_Colonia_Cliente", 
					"ciudad"=>"Direccion_Ciudad_Cliente", 
					"estado"=>"Direccion_Estado_Cliente", 
					"pais"=>"Direccion_Pais_Cliente", 
					"cp"=>"Direccion_Codigo_Postal_Cliente", 
				), 
				"embarque"=>array(
					"calle"=>"Direccion_Calle_del_Embarque_Cliente", 
					"colonia"=>"Direccion_Colonia_del_Embarque_Cliente", 
					"ciudad"=>"Direccion_Ciudad_del_Embarque_Cliente", 
					"estado"=>"Direccion_Estad_del_Embarque_Cliente", 
					"pais"=>"Direccion_Pais_del_Embarque_Cliente", 
					"cp"=>"Direccion_Codigo_Postal_del_Embarque_Cliente", 
					"telefono"=>"Direccion_Telefono_del_Embarque_Cliente"
				), 
				"tarjeta"=>array(
					"id"=>"Identificador_Tarjeta_Cliente_en_tu_Sistema", 
					"card_numero"=>"Numero_Digitos_Tarjeta_Cliente", 
					"card_codigo"=>"CCV_Tarjeta_Cliente", 
					"card_expiracion"=>"mm/yy_Tarjeta_Cliente", 
					"nombre"=>"Nombre_Tarjeta_Cliente", 
					"email"=>"Email_Tarjeta_Cliente", 
					"telefono"=>"Telefono_Tarjeta_Cliente", 
					"nit"=>"Identificacion_Fiscal_Tarjeta_Cliente", 
					"calle"=>"Direccion_Calle_Tarjeta_Cliente", 
					"colonia"=>"Direccion_Colonia_Tarjeta_Cliente", 
					"ciudad"=>"Direccion_Ciudad_Tarjeta_Cliente", 
					"estado"=>"Direccion_Estad_Tarjeta_Cliente", 
					"pais"=>"Direccion_Pais_Tarjeta_Cliente", 
					"cp"=>"Direccion_Codigo_Postal_Tarjeta_Cliente", 
					"telefono"=>"Direccion_Telefono_Tarjeta_Cliente"
				)
			);
		}
		else { # efectivo
			return array(
				"orden_id"=>"Numero_de_Orden_De_Tu_Sistema", 
				"orden_expiracion"=>"Fecha_Expiracion_Linea_de_Cobro", 
				"nombre"=>"Descripcion de la orden", 
				"notify"=>"Url_De_Las_Notificaciones", 
				"total"=>"Monto_Total_de_la_Compra", 
				"pais"=>"Pais_Del_Emisor_del_Cobro", 
				"ip"=>"IP_del_Cliente", 
				"navegador"=>"Nombre_Navegador_Cliente", 
				"cliente"=>array("email"=>"Correo_del_Cliente")
			);
		}
	}

	/**
	* enviar generacion de pago o linea de captura
	*/
	public function sendPayment($data=NULL) {
		if( !$this->getUser() )	$this->setError("no indico un username", "001");
		else if( !$this->getKey() )		$this->setError("no indico el Key de la conexion", "003");
		else if( !$this->getId() )		$this->setError("no indico el identificador de la cuenta", "019");
		else if( !$this->getMerchId() )		$this->setError("no indico el identificador del mercado", "018");
		else if( !$this->isPaymentMethodExist() )	$this->setError("no indico el nombre del metodo de pago", "021");
		else if( !$this->getMoneda() )	$this->setError("no indico el tipo de moneda o divisa", "025");
		else if( !$data || !count($data) )	$this->setError("no indico los datos para el envio del pago", "022");
		else {
			$this->setError(NULL);
			$this->setSucess(NULL);

			$mpago= $this->paymentTypeName;

			if( $this->isCard() ) { # tarjeta
				$params= array(
					"language"=>"es", 
					"command"=>"SUBMIT_TRANSACTION", 
					"merchant"=>array(
						"apiLogin"=>$this->getUser(), 
						"apiKey"=>$this->getKey(), 
					), 
					"transaction"=>array(
						"order"=>array(
							"accountId"=>$this->getId(), 
							"referenceCode"=>$data["orden_id"], 
							"description"=>$data["nombre"], 
							"language"=>"es", 
							"signature"=>$this->getSignature($data["orden_id"],$data["total"]), 
							"notifyUrl"=>$data["notify"], 
							"additionalValues"=>array(
								"TX_VALUE"=>array(
									"value"=>$data["total"], 
									"currency"=>$this->getMoneda()
								)
							),
							"buyer"=>array(
								"merchantBuyerId"=>$data["cliente"]["id"], 
								"fullName"=>$data["cliente"]["nombre"], 
								"emailAddress"=>$data["cliente"]["email"], 
								"contactPhone"=>$data["cliente"]["telefono"], 
								"dniNumber"=>($data["cliente"]["nit"] ? $data["cliente"]["nit"]:0), 
								"shippingAddress"=>array(
									"street1"=>$data["cliente"]["calle"], 
									"street2"=>$data["cliente"]["colonia"], 
									"city"=>$data["cliente"]["ciudad"], 
									"state"=>$data["cliente"]["estado"], 
									"country"=>$data["cliente"]["pais"], 
									"postalCode"=>$data["cliente"]["cp"], 
									"phone"=>$data["cliente"]["telefono"]
								)
							),
							"shippingAddress"=>array(
								"street1"=>$data["embarque"]["calle"], 
								"street2"=>$data["embarque"]["colonia"], 
								"city"=>$data["embarque"]["ciudad"], 
								"state"=>$data["embarque"]["estado"], 
								"country"=>$data["embarque"]["pais"], 
								"postalCode"=>$data["embarque"]["cp"], 
								"phone"=>$data["embarque"]["telefono"]
							)
						), 
						"payer"=>array(
							"merchantPayerId"=>$data["tarjeta"]["id"], 
							"fullName"=>$data["tarjeta"]["nombre"], 
							"emailAddress"=>$data["tarjeta"]["email"], 
							#"birthdate"=>$data["tarjeta"]["fechanacimiento"], 
							"contactPhone"=>$data["tarjeta"]["telefono"], 
							"dniNumber"=>$data["tarjeta"]["nit"], 
							"billingAddress"=>array(
								"street1"=>$data["tarjeta"]["calle"], 
								"street2"=>$data["tarjeta"]["colonia"], 
								"city"=>$data["tarjeta"]["ciudad"], 
								"state"=>$data["tarjeta"]["estado"], 
								"country"=>$data["tarjeta"]["pais"], 
								"postalCode"=>$data["tarjeta"]["cp"], 
								"phone"=>$data["tarjeta"]["telefono"]
							)
						),
						"creditCard"=>array(
							"number"=>$data["tarjeta"]["card_numero"], 
							"securityCode"=>$data["tarjeta"]["card_codigo"], 
							"expirationDate"=>$data["tarjeta"]["card_expiracion"], 
							"name"=>"APPROVED" # REJECTED, APPROVED, DECLINED, PENDING
						),
						"extraParameters"=>array(
							"INSTALLMENTS_NUMBER"=>1
						),
						"type"=>"AUTHORIZATION_AND_CAPTURE", 
						"paymentMethod"=>$mpago, # OXXO, SEVEN_ELEVEN, VISA, MASTERCARD
						"paymentCountry"=>$data["pais"], 
						"deviceSessionId"=>$this->getDeviceSession(), 
						"ipAddress"=>$data["ip"], 
						"cookie"=>session_id(),  # la session del equipo
						"userAgent"=>$data["navegador"]
					),
					"test"=>($this->isCard() ? true:false) # true= tarjetas, false=efectivo
				);
echo '<br><br>sendPayment: TARJETA...<br>';
print_r($params);
			}
			else { # efectivo
				$params= array(
					"language"=>"es", 
					"command"=>"SUBMIT_TRANSACTION", 
					"merchant"=>array(
						"apiLogin"=>$this->getUser(), 
						"apiKey"=>$this->getKey()
					), 
					"transaction"=>array(
						"order"=>array(
							"accountId"=>$this->getId(), 
							"referenceCode"=>$data["orden_id"], 
							"description"=>$data["nombre"], 
							"language"=>"es", 
							"signature"=>$this->getSignature($data["orden_id"],$data["total"]), 
							"notifyUrl"=>$data["notify"], 
							"additionalValues"=>array(
								"TX_VALUE"=>array(
									"value"=>$data["total"], 
									"currency"=>strtoupper($this->getMoneda())
								)
							),
							"buyer"=>array( 
								"fullName"=>$data["cliente"]["nombre"], 
								"emailAddress"=>$data["cliente"]["email"] 
							)
						), 
						"type"=>"AUTHORIZATION_AND_CAPTURE", 
						"paymentMethod"=>$mpago, # OXXO, SEVEN_ELEVEN, VISA, MASTERCARD
						"expirationDate"=>($this->isPSE() ? 0:$data["orden_expiracion"]), 
						"paymentCountry"=>strtoupper($data["pais"]), 
						"deviceSessionId"=>$this->getDeviceSession(), 
						"ipAddress"=>$data["ip"]
					),
					"test"=>($this->isCard() ? true:false) # true= tarjetas, false=efectivo
				);

				if( $data["impuestos"] ) {
					$params["transaction"]["order"]["additionalValues"]["TX_TAX"]= array(
						"value"=>$data["impuestos"], 
						"currency"=>strtoupper($this->getMoneda())
					);
				}
				if( $data["subtotal"] ) {
					$params["transaction"]["order"]["additionalValues"]["TX_TAX_RETURN_BASE"]= array(
						"value"=>$data["subtotal"], 
						"currency"=>strtoupper($this->getMoneda())
					);
				}
				if( $this->isPSE() ) {
					$params["transaction"]["extraParameters"]= $this->getPSEForm(); # obtenemos el form llenado con anterioridad
				}
			}

			$url= self::API_SERVICE;

			$this->sendToPayu($url,json_encode($params), NULL);
			$r= $this->getSucess();

			echo '<br>Medio Pago: '. $mpago;
			echo '<br>Enviando:<br>';
			print_r($params);
			echo '<br><br>Respuesta en API..<br>';
			print_r($r);

			if( $r ) {
				$this->setRespuesta($r);
				$this->setError(NULL);
			}
		}
	}

	/**
	* establece directorio donde guardaremos los recibos, bouchers o lineas de captura
	*
	* @param string $a ruta hacia el lugar donde se guardan los archivos
	*/
	public function setPathReceipt($a=NULL) {
		$this->pathRecibo= ($a ? $a:NULL);
	}

	/**
	* retorna el directorio donde guardaremos los recibos, bouchers o lineas de captura
	*
	* @return string ruta hacia el lugar donde se guardan los archivos
	*/
	public function getPathReceipt($a=NULL) {
		return $this->pathRecibo;
	}

	/**
	* establece nombre del recibo, boucher o linea de captura
	*
	* @param string $a nombre del archivo a guardar
	*/
	public function setReceiptName($a=NULL) {
		$this->reciboName= ($a ? $a:NULL);
	}

	/**
	* retorna el nombre del recibo, boucher o linea de captura
	*
	* @return string nombre del archivo
	*/
	public function getReceiptName($a=NULL) {
		return $this->reciboName;
	}

	/**
	* establece los datos del Emisor
	*
	* @param array $a arreglo de datos de la informacion del emisor
	*/
	public function setEmisorData($a=NULL) {
		$this->empresa= (count($a) ? $a:NULL);
	}

	/**
	* retorna los datos del Emisor
	*
	* @return array arreglo de datos de la informacion del emisor
	*/
	public function getEmisorData() {
		return $this->empresa;	
	}

	/**
	* establece los datos del Receptor
	*
	* @param array $a arreglo de datos de la informacion del receptor
	*/
	public function setReceptorData($a=NULL) {
		$this->cliente= (count($a) ? $a:NULL);
	}

	/**
	* retorna los datos del Receptor
	*
	* @return array arreglo de datos de la informacion del receptor
	*/
	public function getReceptorData() {
		return $this->cliente;	
	}

	/**
	* establece los datos del Cobro
	*
	* @param array $a arreglo de datos de la informacion del cobro
	*/
	public function setCobroData($a=NULL) {
		$this->cobro= (count($a) ? $a:NULL);
	}

	/**
	* retorna los datos del Cobro
	*
	* @return array arreglo de datos de la informacion del cobro
	*/
	public function getCobroData() {
		return $this->cobro;
	}

	/**
	* retorna el formato para generar el recibo imprimible
	*
	* @return string ruta al html a utilizar
	*/
	public function getPrintFormat() {
		return ($this->isCard() ? $this->formatoTarjeta:$this->formatoEfectivo);
	}

	/**
	* retorna el formulario para presentar visualmente en formato array
	*
	* @return array formulario de datos
	*/
	public function getFormPasarel() {
		if( !$this->needPaymentPasarel() )
			return false;
		else {
			if( $this->isCard() ) {
				return true;
			}
			else if( !strcmp(strtolower($this->getPaymentName()), "pse") ) {
				$this->getBankList();
				return true;
			}
		}
	}

	/**
	* retorna las variables que se deben capturar del formulario
	*
	* @return array variables POST a capturar
	*/
	public function getFormPostVarsPasarel() {
		if( !$this->needPaymentPasarel() )
			return 0;
		else if( !strcmp(strtolower($this->getPaymentName()), "pse") ) {
			return array(
				"RESPONSE_URL"=>"http://www.test.com/response",
				"PSE_REFERENCE1"=>"127.0.0.1",
				"FINANCIAL_INSTITUTION_CODE"=>"1007",
				"USER_TYPE"=>"N",
				"PSE_REFERENCE2"=>"CC",
				"PSE_REFERENCE3"=>"123456789"
			);
		}
	}

	/**
	* imprime recibo, boucher o linea de captura en estado recibido, aprobado o rechazado..
	*/
	public function printReceipt() {
		if( !$this->getUser() )	$this->setError("no indico un username", "001");
		else if( !$this->getKey() )		$this->setError("no indico el Key de la conexion", "003");
		else if( !$this->getId() )		$this->setError("no indico el identificador de la cuenta", "019");
		else if( !$this->getMerchId() )		$this->setError("no indico el identificador del mercado", "018");
		else if( !$this->isPaymentMethodExist() )	$this->setError("no indico el nombre del metodo de pago", "021");
		else if( !$this->getPathReceipt() )		$this->setError("no indico la ruta o path donde se guardara el archivo", "026");
		else if( !$this->getReceiptName() )		$this->setError("no indico el nombre del archivo", "027");
		else if( !is_array($this->getEmisorData()) )		$this->setError("no indico los datos del Emisor", "028");
		else if( !is_array($this->getReceptorData()) )	$this->setError("no indico los datos del Receptor", "029");
		else if( !is_array($this->getCobroData()) )		$this->setError("no indico los datos del Cobro", "030");
		else {
			$tmp= $this->getPrintFormat(); # ruta al HTML
			$html_fact= $this->getPathReceipt().$this->getReceiptName().'.html'; # archivo HTML
			$pdf_fact= $this->getPathReceipt().$this->getReceiptName().'.pdf'; # archivo PDF
			$total=0;
			$impuesto=0;
			$moneda= $this->getMoneda();
			$labase= ($this->cobro["ID_COTIZACION"] ? "COTIZACIONES":($this->cobro["ID_FACTURA"] ? "FACTURACION":0));

			if( !$labase ) {
				$total= $this->cobro["REFERENCIA_MANUAL"];
				$detallecompra= 'Se genero referencia de cobro de forma manual..';
			}
			else {
				$patron= '@\[([0-9a-zA-Z]{1,})\]@';
				preg_match_all($patron, ($this->cobro["ID_COTIZACION"] ? $this->cobro["ID_COTIZACION"]:($this->cobro["ID_FACTURA"] ? $this->cobro["ID_FACTURA"]:0)), $out);

				foreach( $out[0] as $key=>$val ) {
					$total += mbnumber_format(consultar_datos_general( $labase, "ID='". proteger_cadena($out[1][$key]). "' && ID_USUARIO='". $_SESSION["SUPERID"]. "'", "TOTAL" ), 2, '.', '');
					$tmp_imp= consultar_datos_general($labase, "ID='". proteger_cadena($out[1][$key]). "' && ID_USUARIO='". $_SESSION["SUPERID"]. "'", "IMPUESTO");
					$impuesto += ($tmp_imp ? $tmp_imp:0); # sumamos el impuesto
					$dt= init_conceptos("hash2arr", consultar_datos_general($labase, "ID='". proteger_cadena($out[1][$key]). "' && ID_USUARIO='". $_SESSION["SUPERID"]. "'", "DESCRIPCION"));

					foreach( $dt as $key2=>$val2 ) {
						$detallecompra .= '<div class="w3-row"><div class="w3-third w3-col s2 m2 l2">'. $val2["cantidad"]. '</div><div class="w3-third w3-col s6 m6 l6">'. desproteger_cadena($val2["concepto"]). '</div><div class="w3-third w3-right-align w3-col s4 m4 l4">'. mbnumber_format(($val2["pu"]*$val2["cantidad"]), 2, '.', ','). ' '. $moneda. '</div></div>';
					}
					unset($tmp_imp);
				}

				$detallecompra .= ($impuesto ? '<div class="W3-row"><div class="w3-half w3-left-align w3-col s6 m6 l6">Impuestos:</div><div class="w3-half w3-right-align w3-col s6 m6 l6">'. mbnumber_format($impuesto, 2, '.', ','). ' '. $moneda. '</div></div>':'');
			}

			$html= file_get_contents($tmp); # obtenemos el stream del HTML
			$codif= ( strstr($this->getImagePaymentMethod(), '.jpg') ? 'data:image/jpg;base64,':(strstr($this->getImagePaymentMethod(), '.png') ? 'data:image/png;base64,':(strstr($this->getImagePaymentMethod(), '.webp') ? 'data:image/webp;base64,':NULL)));
			$html= preg_replace( '/\[IMAGEN_VENDOR\]/', $codif.base64_encode(file_get_contents($this->getImagePaymentMethod())), $html );
			$html= preg_replace( '/\[CODIGO_BARRAS_NUMERO\]/', desproteger_cadena(wordwrap($this->cobro["MENSAJE_EMISION"], 4, "-", true)), $html );
			$html= preg_replace( '/\[CODIGO_BARRAS\]/', '<img src="'. HTTP_SERVER.'/'.substr($this->cobro["FILE_PDF"], 0, -4). '_barcode.jpg" border="0" style="width:100%;">', $html );
			$html= preg_replace( '/\[ORDEN_COMPRA\]/', desproteger_cadena($this->cobro["ID_CONTROL"]), $html );
			$html= preg_replace( '/\[TOTAL\]/', mbnumber_format($total, 2, '.', ','). ' '. $moneda, $html );
			$html= preg_replace( '/\[TIENDA\]/', desproteger_cadena($this->cobro["TIENDA_NAME"]), $html );
			$html= preg_replace( '/\[EMPRESA_EMISORA\]/', desproteger_cadena(consultar_datos_general("USUARIOS", "ID='". $this->cobro["ID_USUARIO"]. "'", "EMPRESA")), $html );
			$html= preg_replace( '/\[FECHA_COMPRA\]/', date( "d/m/Y, H:i:s a", $this->cobro["FECHA"]), $html );
			$html= preg_replace( '/\[FECHA_VENCIMIENTO\]/', date( "d/m/Y, H:i:s a", $this->cobro["FECHA_EXPIRACION"]), $html );
			$html= preg_replace( '/\[TITULO_COMPRA\]/', "Compra ". $this->cobro["ID_CONTROL"]. " el ". date("d/m/Y, g:i a", $this->cobro["FECHA"]), $html );
			$html= preg_replace( '/\[TRANSACCION\]/', desproteger_cadena($this->cobro["MENSAJE_EMISION"]), $html );
			$html= preg_replace( '/\[DETALLE_COMPRA\]/', $detallecompra, $html );

			# datos del cliente
			$html= preg_replace( '/\[CLIENTE_NOMBRE\]/', desproteger_cadena($this->cliente["NOMBRE"]), $html );
			$html= preg_replace( '/\[CLIENTE_EMAIL\]/', desproteger_cadena($this->cliente["EMAIL"]), $html );
			$html= preg_replace( '/\[CLIENTE_RFC\]/', desproteger_cadena($this->cliente["RFC"] ? $this->cliente["RFC"]:''), $html );
			$html= preg_replace( '/\[CIUDAD\]/', desproteger_cadena(get_clienteprofile($this->cliente["CIUDAD"], "sat_ciudades", 0)), $html );
			$html= preg_replace( '/\[ESTADO\]/', desproteger_cadena(get_clienteprofile($this->cliente["ESTADO"], "sat_estados", 0)), $html );
			$html= preg_replace( '/\[PAIS\]/', desproteger_cadena(get_clienteprofile($this->cliente["PAIS"], "sat_paises", 0)), $html );
			$html= preg_replace( '/\[REFERENCIA\]/', desproteger_cadena($this->cobro["ID_PASARELA"]), $html );

			if( $this->isCard() ) { # datos para tarjetas
				$html= preg_replace( '/\[TARJETA_ESTATUS\]/', desproteger_cadena(($this->cobro["ESTADO"]==5) ? 'DECLINADA':'APROBADA'), $html );
				$html= preg_replace( '/\[ID_TRANSACCION\]/', desproteger_cadena($this->cobro["TARJETA_RESPONSE"]), $html );
				$html= preg_replace( '/\[TARJETA_NUMAP\]/', desproteger_cadena($this->cobro["TARJETA_NUMAP"] ? $this->cobro["TARJETA_NUMAP"]:'--'), $html );
				$html= preg_replace( '/\[TARJETA_CODIGOAP\]/', desproteger_cadena($this->cobro["TARJETA_CODIGOAP"] ? $this->cobro["TARJETA_CODIGOAP"]:'--'), $html );
				$html= preg_replace( '/\[TARJETA_4DIGITOS\]/', substr( desproteger_cadena(consultar_datos_general( "TARJETAS", "ID='". proteger_cadena($this->cobro["ID_TARJETA"]). "' && ID_USUARIO='". $_SESSION["SUPERID"]. "'", "CUENTA")), -4), $html );
				$html= preg_replace( '/\[TARJETA_TIPO\]/', desproteger_cadena(get_clienteprofile(consultar_datos_general( "TARJETAS", "ID='". proteger_cadena($this->cobro["ID_TARJETA"]). "' && ID_USUARIO='". $_SESSION["SUPERID"]. "'", "TIPO"), "tarjeta_tipo", 0)), $html );
				$html= preg_replace( '/\[TARJETA_BANCO\]/', desproteger_cadena(get_empleadoprofile(consultar_datos_general( "TARJETAS", "ID='". proteger_cadena($this->cobro["ID_TARJETA"]). "' && ID_USUARIO='". $_SESSION["SUPERID"]. "'", "BANCO"), "bancos", 0)), $html );

				$html= preg_replace( '/\[EMPRESA_NOMBRE\]/', desproteger_cadena($this->empresa["EMPRESA"] ? $this->empresa["EMPRESA"]:$this->empresa["NOMBRE"]), $html );
				$html= preg_replace( '/\[EMPRESA_RFC\]/', desproteger_cadena($this->empresa["RFC"]), $html );
				$html= preg_replace( '/\[EMPRESA_CALLE\]/', desproteger_cadena($this->empresa["CALLE"]), $html );
				$html= preg_replace( '/\[EMPRESA_NUMEXT\]/', desproteger_cadena($this->empresa["NUM_EXT"]), $html );
				$html= preg_replace( '/\[EMPRESA_CP\]/', desproteger_cadena($this->empresa["CP"]), $html );
				$html= preg_replace( '/\[EMPRESA_COLONIA\]/', desproteger_cadena($this->empresa["COLONIA"]), $html );
				$html= preg_replace( '/\[EMPRESA_CIUDAD\]/', desproteger_cadena(get_clienteprofile($this->empresa["CIUDAD"], "sat_ciudades", 0)), $html );
				$html= preg_replace( '/\[EMPRESA_ESTADO\]/', desproteger_cadena(get_clienteprofile($this->empresa["ESTADO"], "sat_estados", 0)), $html );
				$html= preg_replace( '/\[EMPRESA_PAIS\]/', desproteger_cadena(get_clienteprofile($this->empresa["PAIS"], "sat_paises", 0)), $html );
				$html= preg_replace( '/\[EMPRESA_TELEFONO\]/', desproteger_cadena($this->empresa["TELEFONO"]), $html );
			}

			$fp= fopen( $html_fact, "w" ); # creamos archivo html
			fwrite($fp, $html );	# guardamos datos html
			fclose($fp);	# cerramos stream
			unset($fp);
			system( '/usr/bin/wkhtmltopdf '. $html_fact. ' '. $pdf_fact );	# creamos PDF
			$this->setRespuesta("representaci&oacute;n grafica generada con exito");
			$this->setError(NULL);
		}
	}

	/**
	* constructor de la clase PayU
	*/
	public function __construct($user=NULL, $key=NULL, $id=NULL, $merchId=NULL, $pais=NULL, $m=NULL, $ret=3, $sandbox=NULL) {
		$this->setSanbox($sandbox);
		$this->setUser($user);
		$this->setKey($key);
		$this->setId($id);
		$this->setMerchId($merchId);
		$this->setCountry($pais);
		$this->setMoneda($m);
		$this->setRetryTime($ret);
		$this->setWebserviceAPI();
	}
}
?>