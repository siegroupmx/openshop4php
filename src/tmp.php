<?php
# send a payment
function payu_sendpayment( $data )
	{
	if( !is_array($data) || !count($data) )		return 0;
	else if( !$data["type"] )		return 0;
	else if( strcmp($data["type"], "visa") && strcmp($data["type"], "mastercard") && strcmp($data["type"], "seven_eleven") && strcmp($data["type"], "oxxo") ) 	return 0;
	else
		{
		# print_r($data);
		if( !strcmp($data["type"], "visa") )	$mpago= 'VISA';
		else if( !strcmp($data["type"], "mastercard") )		$mpago= 'MASTERCARD';
		else if( !strcmp($data["type"], "seven_eleven") )	$mpago= 'SEVEN_ELEVEN';
		else if( !strcmp($data["type"], "oxxo") )	$mpago= 'OXXO';

		if( !strcmp($data["type"], "oxxo") || !strcmp($data["type"], "seven_eleven") ) # pago en efectivo
			{
			$query= array(
				"language"=>"es", 
				"command"=>"SUBMIT_TRANSACTION", 
				"merchant"=>array(
					"apiLogin"=>PAYU_USER, 
					"apiKey"=>PAYU_KEY
					), 
				"transaction"=>array(
					"order"=>array(
						"accountId"=>PAYU_ID, 
						"referenceCode"=>$data["orden_id"], 
						"description"=>$data["nombre"], 
						"language"=>"es", 
						"signature"=>payu_signature($data["orden_id"],$data["total"],$data["moneda"]), 
						"notifyUrl"=>$data["notify"], 
						"additionalValues"=>array(
							"TX_VALUE"=>array(
								"value"=>$data["total"], 
								"currency"=>$data["moneda"]
								)
							),
						"buyer"=>array( "emailAddress"=>$data["cliente"]["email"] )
						), 
					"type"=>"AUTHORIZATION_AND_CAPTURE", 
					"paymentMethod"=>$mpago, # OXXO, SEVEN_ELEVEN, VISA, MASTERCARD
					"expirationDate"=>$data["orden_expiracion"], 
					"paymentCountry"=>$data["pais"], 
					"ipAddress"=>$data["ip"], 
					),
				"test"=>((!strcmp($mpago, "OXXO") || !strcmp($mpago, "SEVEN_ELEVEN")) ? false:true) # true= tarjetas, false=efectivo
				);
			}
		else # en tarjeta
			{
			$query= array(
				"language"=>"es", 
				"command"=>"SUBMIT_TRANSACTION", 
				"merchant"=>array(
					"apiLogin"=>PAYU_USER, 
					"apiKey"=>PAYU_KEY
					), 
				"transaction"=>array(
					"order"=>array(
						"accountId"=>PAYU_ID, 
						"referenceCode"=>$data["orden_id"], 
						"description"=>$data["nombre"], 
						"language"=>"es", 
						"signature"=>payu_signature($data["orden_id"],$data["total"],$data["moneda"]), 
						"notifyUrl"=>$data["notify"], 
						"additionalValues"=>array(
							"TX_VALUE"=>array(
								"value"=>$data["total"], 
								"currency"=>$data["moneda"]
								)
							),
						"buyer"=>array(
							"merchantBuyerId"=>$data["cliente"]["id"], 
							"fullName"=>$data["cliente"]["nombre"], 
							"emailAddress"=>$data["cliente"]["email"], 
							"contactPhone"=>$data["cliente"]["telefono"], 
							"dniNumber"=>($data["cliente"]["nit"] ? $data["cliente"]["nit"]:0), 
							"shippingAddress"=>array(
								"street1"=>$data["cliente"]["calle"], 
								"street2"=>$data["cliente"]["colonia"], 
								"city"=>$data["cliente"]["ciudad"], 
								"state"=>$data["cliente"]["estado"], 
								"country"=>$data["cliente"]["pais"], 
								"postalCode"=>$data["cliente"]["cp"], 
								"phone"=>$data["cliente"]["telefono"]
								)
							),
						"shippingAddress"=>array(
							"street1"=>$data["embarque"]["calle"], 
							"street2"=>$data["embarque"]["colonia"], 
							"city"=>$data["embarque"]["ciudad"], 
							"state"=>$data["embarque"]["estado"], 
							"country"=>$data["embarque"]["pais"], 
							"postalCode"=>$data["embarque"]["cp"], 
							"phone"=>$data["embarque"]["telefono"]
							)
						), 
					"payer"=>array(
						"merchantPayerId"=>$data["tarjeta"]["id"], 
						"fullName"=>$data["tarjeta"]["nombre"], 
						"emailAddress"=>$data["tarjeta"]["email"], 
						#"birthdate"=>$data["tarjeta"]["fechanacimiento"], 
						"contactPhone"=>$data["tarjeta"]["telefono"], 
						"dniNumber"=>$data["tarjeta"]["nit"], 
						"billingAddress"=>array(
							"street1"=>$data["tarjeta"]["calle"], 
							"street2"=>$data["tarjeta"]["colonia"], 
							"city"=>$data["tarjeta"]["ciudad"], 
							"state"=>$data["tarjeta"]["estado"], 
							"country"=>$data["tarjeta"]["pais"], 
							"postalCode"=>$data["tarjeta"]["cp"], 
							"phone"=>$data["tarjeta"]["telefono"]
							)
						),
					"creditCard"=>array(
						"number"=>$data["tarjeta"]["card_numero"], 
						"securityCode"=>$data["tarjeta"]["card_codigo"], 
						"expirationDate"=>$data["tarjeta"]["card_expiracion"], 
						"name"=>"APPROVED" # REJECTED, APPROVED, DECLINED, PENDING
						),
					"extraParameters"=>array(
						"INSTALLMENTS_NUMBER"=>1
						),
					"type"=>"AUTHORIZATION_AND_CAPTURE", 
					"paymentMethod"=>$mpago, # OXXO, SEVEN_ELEVEN, VISA, MASTERCARD
					"paymentCountry"=>$data["pais"], 
					"deviceSessionId"=>$data["device"], 
					"ipAddress"=>$data["ip"], 
					"cookie"=>$data["cookie"],  # la session del equipo
					"userAgent"=>$data["navegador"]
					),
				"test"=>((!strcmp($mpago, "OXXO") || !strcmp($mpago, "SEVEN_ELEVEN")) ? false:true) # true= tarjetas, false=efectivo
				);
			}

		# print_r($query);

		$data= array(
			"POST", 
			PAYU_SERVICE,
			"Accept: application/json\r\nContent-Length: ". strlen(json_encode($query)). "\r\n", 
			"json", 
			$query
			);

		$a= socket_iodata(PAYU_API_SANDBOX, $data, 443);
		$a= explode("\r\n\r\n", $a);
		$a= explode("\r\n", $a[1]);
		$r= json_decode($a[1]);
		unset($a, $data, $query);

		# echo '<br>Data: ['. $tr. ']<br>';
		# print_r($r);
		# echo '<br>resultado transaccion: '. (count($r->result) ? 'Exito':'Fallo');
		}
	return $r;
	}

function payu( $op=NULL, $data=NULL )
	{
	$r=0;

	if( !strcmp($op, "get_pmethods") )
		$r= payu_paymentMethods($data);
	else if( !strcmp($op, "get_order") )
		$r= payu_orders($data);
	else if( !strcmp($op, "get_transaction") )
		$r= payu_transactions($data);
	else if( !strcmp($op, "send_payment") && count($data) )
		$r= payu_sendpayment($data);

	return $r;
	}
?>